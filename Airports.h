//
//  Airports.h
//  Radose
//
//  Created by mm7 on 19.07.14.
//  Copyright (c) 2014 mm7. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Airports : NSManagedObject
@property (nonatomic, retain) NSString * city_eng;
@property (nonatomic, retain) NSString * city_rus;
@property (nonatomic, retain) NSString * country_eng;
@property (nonatomic, retain) NSString * country_rus;

@property (nonatomic, retain) NSString * iata_code;
@property (nonatomic, retain) NSString * icao_code;
@property (nonatomic, retain) NSString * name_eng;
@property (nonatomic, retain) NSString * name_rus;

-(void)setDict:(NSDictionary*)dict;
@end
