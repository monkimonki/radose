#define locKeyAbout @"loc.about"
#define locKeyDPF @"loc.dpf"
#define locKeyStat @"loc.stat"
#define locKeyAccount @"loc.acc"
#define locDone @"loc.done"

//calculator
#define locFlightTime @"loc.flightTime"
#define locDepart @"loc.depart"
#define locArrive @"loc.arrive"


#define locClear @"loc.clear"
#define locCalculate @"loc.calculate"


#define locSelect @"loc.select"

#define locTime @"loc.time"
#define locAirport @"loc.airport"
#define locDate @"loc.date"

#define locClear @"loc.clear"
#define locCalculate @"loc.calculate"

#define locSearchPlaceHolder @"loc.searchplaceholder"

#define locAirPortsUpdating @"loc.airportupdating"


#define locOfDaylyRate @"loc.ofdaylyrate"

#define locSaveStatisticMessage @"loc.savestatmessage"