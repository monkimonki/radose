//
//  StartViewController.m
//  Radose
//
//  Created by mm7 on 17.07.14.
//  Copyright (c) 2014 mm7. All rights reserved.
//

#import "StartViewController.h"
#import "RDTextField.h"
#import "RDButton.h"
#import "RemindPasswordViewController.h"
#import "RegisterViewController.h"
#import "OrVIew.h"
#import "RDTabBarViewController.h"
@interface StartViewController (){
    RDTextField * loginTextField;
    RDTextField * passwordTextField;
}

@end

@implementation StartViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
   
    loginTextField=[RDTextField createBase];
    [loginTextField setPlaceholder:@"email"];
    [loginTextField setCenter:CGPointMake(self.view.frame.size.width/2,self.view.frame.size.height/2)];
    [loginTextField setVvTextFieldDelegate:self];
    [self.view addSubview:loginTextField];
   
    
    passwordTextField =[RDTextField createBase];
    [passwordTextField setPlaceholder:@"password"];
    [passwordTextField setSecureTextEntry:YES];
    [passwordTextField setVvTextFieldDelegate:self];
    [passwordTextField setCenter:CGPointMake(self.view.frame.size.width/2, loginTextField.frame.origin.y+loginTextField.frame.size.height+passwordTextField.frame.size.height/2+5)];
    
    RDButton * but=[RDButton createWithTitle:@"Sign In"];
    
    
    [but setCenter:CGPointMake(self.view.frame.size.width/2, passwordTextField.frame.size.height+passwordTextField.frame.origin.y+22+but.frame.size.height/2)];
    
    [self.view addSubview:but];
    
    
    
    
    [loginTextField addImage:[UIImage imageNamed:@"email_ico"]];
    [passwordTextField addImage:[UIImage imageNamed:@"lock"]];
    
    UIButton * butCreateAcc=[UIButton buttonWithType:UIButtonTypeCustom];
    
    [butCreateAcc setFrame:CGRectMake(10, but.frame.size.height+but.frame.origin.y+15, 288/2, 92/2)];
    [butCreateAcc setTitle:@"Create account" forState:UIControlStateNormal];
    [butCreateAcc.titleLabel setFont:[UIFont RDFontMedium]];
    [butCreateAcc setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [butCreateAcc setTitleColor:[UIColor colorWithRed:1 green:1 blue:1 alpha:0.5  ] forState:UIControlStateHighlighted];
    [butCreateAcc setAlpha:0];
    [butCreateAcc addTarget:self action:@selector(createAcc) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:butCreateAcc];
    
    
    UIButton * butForgetPass=[UIButton buttonWithType:UIButtonTypeCustom];
    
    [butForgetPass setFrame:CGRectMake(self.view.frame.size.width-10-288/2, but.frame.size.height+but.frame.origin.y+15, 288/2, 92/2)];
    [butForgetPass setTitle:@"forget password?" forState:UIControlStateNormal];
    [butForgetPass.titleLabel setFont:[UIFont RDFontMedium]];
    [butForgetPass setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [butForgetPass setTitleColor:[UIColor colorWithRed:1 green:1 blue:1 alpha:0.5  ] forState:UIControlStateHighlighted];
    [butForgetPass setAlpha:0];
    [self.view addSubview:butForgetPass];
    
    
    OrVIew * v=[[OrVIew alloc]initWithFrame:CGRectMake(self.view.frame.size.width/2-46/2, butForgetPass.frame.origin.y,46, butForgetPass.frame.size.height)];
    [v setBackgroundColor:[UIColor clearColor]];
    [self.view addSubview:v];
    
    [but setAlpha:0];
    [but addTarget:self action:@selector(signIn) forControlEvents:UIControlEventTouchUpInside];
    [butForgetPass addTarget:self action:@selector(remindPass) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:passwordTextField];
    [self addAlphaShowView:loginTextField];
    [self addAlphaShowView:passwordTextField];
    [[RDNavigationController sharedInstance]showLogoWithAnimation:YES andCompl:^{
            [self showAlphaArr:^{
      [UIView animateWithDuration:0.33 animations:^{
          [but setAlpha:1];
          [v anim];
          [butCreateAcc setAlpha:1];
          [butForgetPass setAlpha:1];
      }];
                }];
    }];
    
    
    
    //Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
#pragma mark -actions
-(void)createAcc{
    [self.navigationController pushViewController:[[RegisterViewController alloc] init] animated:YES];
    
}
-(void)signIn{
    
    [[RDSingleton sharedInstance] loginWithEmail:loginTextField.text andPassword:passwordTextField.text andCompBloc:^(BOOL suc, RDErorr *er) {
        
    }];
    
    if (!loginTextField.text) {
        [loginTextField setErrorString:@"enter email"];
        return;
    }
    
    if (![[RDSingleton sharedInstance] checkEmail:loginTextField.text]) {
        
        [loginTextField setErrorString:@"enter valid email"];
        return;
        
    }
    if (!passwordTextField.text) {
        [passwordTextField setErrorString:@"enter password"];
        return;
    }
    
    [[RDSingleton sharedInstance] loginWithEmail:loginTextField.text andPassword:passwordTextField.text andCompBloc:^(BOOL suc, RDErorr *er) {
       
        if (suc) {
            RDTabBarViewController *t=[[RDTabBarViewController alloc] init];
            
            [self.navigationController setViewControllers:[NSArray arrayWithObject:t] animated:YES];
        }
        
    }];
}
-(void)remindPass{
    [self.navigationController pushViewController:[[RemindPasswordViewController alloc] init] animated:YES];

}
@end
