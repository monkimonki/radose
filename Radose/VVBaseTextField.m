//
//  EKTextfield.m
//  TutoringOnDemand
//
//  Created by mm7 on 13.03.14.
//  Copyright (c) 2014 mm7. All rights reserved.
//

#import "VVBaseTextField.h"
@interface VVBaseTextField(){
    
 
}
@end
#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)
@implementation VVBaseTextField

-(id) initWithFrame: (CGRect) frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        //[self setDelegate:self];
        [self setAutocorrectionType:UITextAutocorrectionTypeNo];
        _tfHelper=[[VVTextFieldHelper alloc]init];
        self.delegate= _tfHelper;
        [_tfHelper setDelegate:self];
        _leftOffset=0;
		_rightOffset=0;
        
        
        self.placeHolderColor=[UIColor grayColor];
        _staticTextColor=[UIColor blackColor];
        // Initialization code
        
     //   if (self.keyboardType ==UIKeyboardTypeDecimalPad||self.keyboardType== UIKeyboardTypeNumberPad) {
            UIBarButtonItem *doneItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(doneButtonDidPressed:)];
            UIBarButtonItem *flexableItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:NULL];
            UIToolbar *toolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, [[UIScreen mainScreen] bounds].size.width, 44)];
            [toolbar setItems:[NSArray arrayWithObjects:flexableItem,doneItem, nil]];
            self.inputAccessoryView = toolbar;
            
     //   }
    }
    return self;
}
-(void)setManyTextFields:(BOOL)manyTextFields{
    UIBarButtonItem *doneItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(doneButtonDidPressed:)];

    UIButton * but=[UIButton buttonWithType:UIButtonTypeCustom];
    [but setFrame:CGRectMake(0, 0, 44, 44)];
    [but setImage:[UIImage imageNamed:@"ico-back"] forState:UIControlStateNormal];
    [but addTarget:self action:@selector(tabBack) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *perv=[[UIBarButtonItem alloc]initWithCustomView:but];
    UIButton * but2=[UIButton buttonWithType:UIButtonTypeCustom];
    [but2 setFrame:CGRectMake(0, 0, 44, 44)];
    [but2 setImage:[UIImage imageNamed:@"ico-forward"] forState:UIControlStateNormal];
    [but2 addTarget:self action:@selector(tabForward) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *next= [[UIBarButtonItem alloc] initWithCustomView:but2];
    
    UIBarButtonItem *flexableItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:NULL];
    UIToolbar *toolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, [[UIScreen mainScreen] bounds].size.width, 44)];
    [toolbar setItems:[NSArray arrayWithObjects:perv,next,flexableItem,doneItem, nil]];
    self.inputAccessoryView = toolbar;

    
    
}
-(void)tabBack{
    [self.vvTextFieldDelegate textFieldShouldTabBack:self];
}
-(void)tabForward{
    [self.vvTextFieldDelegate textFieldShouldTabForward:self];
}

-(id)initWithCoder:(NSCoder *)aDecoder{
   
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self setAutocorrectionType:UITextAutocorrectionTypeNo];
        _tfHelper=[[VVTextFieldHelper alloc]init];
         self.delegate= _tfHelper;
        [_tfHelper setDelegate:self];
        // Initialization code
        _leftOffset=0;
		_rightOffset=0;


        self.placeHolderColor=[UIColor grayColor];
          _staticTextColor=[UIColor blackColor];
        
//          
//        if (self.keyboardType ==UIKeyboardTypeDecimalPad||self.keyboardType==UIKeyboardTypeNumberPad) {
            UIBarButtonItem *doneItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(doneButtonDidPressed:)];
            UIBarButtonItem *flexableItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:NULL];
            UIToolbar *toolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, [[UIScreen mainScreen] bounds].size.width, 44)];
            [toolbar setItems:[NSArray arrayWithObjects:flexableItem,doneItem, nil]];
            self.inputAccessoryView = toolbar;
            
      //  }
         
    }
    return self;
    
}
-(void) drawPlaceholderInRect:(CGRect)rect  {
     if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
    if (self.placeholder)
    {
        
        // color of placeholder text
        UIColor *placeHolderTextColor = _placeHolderColor;
        
        CGSize drawSize = [self.placeholder sizeWithAttributes:[NSDictionary dictionaryWithObject:self.font forKey:NSFontAttributeName]];
        CGRect drawRect = rect;
        
        // verticially align text
        drawRect.origin.y = (rect.size.height - drawSize.height) * 0.5;
        
        // set alignment
        NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
        paragraphStyle.alignment = self.textAlignment;
        
        // dictionary of attributes, font, paragraphstyle, and color
        NSDictionary *drawAttributes = @{NSFontAttributeName: self.font,
                                         NSParagraphStyleAttributeName : paragraphStyle,
                                         NSForegroundColorAttributeName : placeHolderTextColor};
        
        
        // draw
        [self.placeholder drawInRect:drawRect withAttributes:drawAttributes];
        
    }
     }
}

-(void)setPlaceHolderColor:(UIColor *)placeHolderColor{
    
    _placeHolderColor=placeHolderColor;

   //     self.attributedPlaceholder = [[NSAttributedString alloc] initWithString:_placeholderString attributes:@{NSForegroundColorAttributeName: _placeHolderColor}];
   // [[UILabel appearanceWhenContainedIn:[UITextField class], nil] setTextColor:_placeHolderColor];
}
-(void)setStaticText:(NSString *)staticText{
    if (staticlabel) {
        [staticlabel removeFromSuperview];
        staticlabel=nil;
    }
    CGSize s=[[NSString stringWithFormat:@"%@ ",staticText] sizeWithAttributes:
              @{NSFontAttributeName:
                    self.font}];

    
    staticlabel=[[UILabel alloc]initWithFrame:CGRectMake(5, 0, s.width, self.frame.size.height)];
    [staticlabel setTextColor:_staticTextColor];
    [staticlabel setFont:self.font];
    [staticlabel setBackgroundColor:[UIColor clearColor]];
    [staticlabel setText:staticText];
    _staticText=staticText;
    
    [self addSubview:staticlabel];
    if (_leftOffset>0 || _rightOffset>0) {
        [self setNewStaticLabelFrame];
    }
    
}
-(void)setLeftOffset:(float)leftOffset{
    _leftOffset=leftOffset;
    if (staticlabel) {
        [self setNewStaticLabelFrame];
    }
}

-(void)setRightOffset:(float)rightOffset{
	_rightOffset=rightOffset;
	if (staticlabel) {
		[self setNewStaticLabelFrame];
	}
}

-(void)setNewStaticLabelFrame{
    [staticlabel setFrame:CGRectMake(staticlabel.frame.origin.x+_leftOffset, staticlabel.frame.origin.y, staticlabel.frame.size.width - (_leftOffset + _rightOffset), staticlabel.frame.size.height)];
}
-(CGRect)boundsWithOffset:(CGRect)bouds{
    return CGRectMake(bouds.origin.x+ _leftOffset, bouds.origin.y, bouds.size.width-(_leftOffset + _rightOffset) ,bouds.size.height);
}

-(CGRect)textRectForBounds:(CGRect)bounds{
    
    CGRect newBounds=[self boundsWithOffset:bounds];
    if (staticlabel) {
        
        return CGRectMake( staticlabel.frame.origin.x+staticlabel.frame.size.width,0,newBounds.size.width- staticlabel.frame.size.width-staticlabel.frame.origin.x, newBounds.size.height) ;
    }
    else return newBounds;
}
-(CGRect)editingRectForBounds:(CGRect)bounds{
        CGRect newBounds=[self boundsWithOffset:bounds];
   if (staticlabel) {
        return CGRectMake(newBounds.origin.x+staticlabel.frame.origin.x+staticlabel.frame.size.width,0, newBounds.size.width- staticlabel.frame.size.width-staticlabel.frame.origin.x, bounds.size.height) ;
    }
    else return newBounds;
}
/*- (void) drawPlaceholderInRect:(CGRect)rect {
    [_placeHolderColor setFill];
    [[self placeholder] drawInRect:rect withFont:self.font ];
}*/

-(void)setBaseColor:(UIColor *)baseColor {
    _baseColor = baseColor;
    placeholderColor  =[UIColor grayColor];
   // self.textColor = placeholderColor;
}
-(void)setPlaceholderString:(NSString *)placeholderString{
  //  self.text=placeholderString;
    _placeholderString=placeholderString;

    
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

#pragma mark -UITextFieldDelegate
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    if ([self.vvTextFieldDelegate respondsToSelector:@selector(editingWithText:)]) {
        
        NSString * newStr;
        if (textField.text.length>0&&[string isEqualToString:@""]) {
            
            newStr=[textField.text substringToIndex:textField.text.length-1];
        }
        else newStr=[NSString stringWithFormat:@"%@%@",textField.text,string];
        


        [self.vvTextFieldDelegate editingWithText:newStr];
    }
    return YES;
    
}
-(BOOL)shouldChangeTextInRange:(UITextRange *)range replacementText:(NSString *)text{
    
    if ([self.vvTextFieldDelegate respondsToSelector:@selector(editingWithText:)]) {
        [self.vvTextFieldDelegate editingWithText:self.text];
    }
    return YES;
    
}
-(void)textFieldDidBeginEditing:(UITextField *)textField {
    if ([textField.text isEqualToString:_placeholderString] ) {
        textField.text = @"";
   
    }
         textField.textColor = _baseColor;
    [self.vvTextFieldDelegate editingBegin:self];
}
-(BOOL)textFieldShouldClear:(UITextField *)textField{
    return YES;
}
-(void) textFieldDidEndEditing:(UITextField *)textField {
    if ([textField.text isEqualToString:_placeholderString] || [textField.text isEqualToString:@""] ) {
      //  textField.text = _placeholderString;
       textField.textColor = placeholderColor;
    }
    [self resignFirstResponder] ;
     textField.textColor = _baseColor;
    [self.vvTextFieldDelegate editingEnd];
    
    if ([self.vvTextFieldDelegate respondsToSelector:@selector(editingEndWithView:)]) {
          [self.vvTextFieldDelegate editingEndWithView:self];
    }
  
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField {
    [self resignFirstResponder] ;
    return YES;
}

#pragma mark - ()
-(NSString *)text{
    NSString * str=[super text];
 
    
        return str;
    
    
}

-(void)setKeyboardType:(UIKeyboardType)keyboardType{
    
    
    if (keyboardType ==UIKeyboardTypeDecimalPad||keyboardType==UIKeyboardTypeNumberPad) {
        UIBarButtonItem *doneItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(doneButtonDidPressed:)];
        UIBarButtonItem *flexableItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:NULL];
        UIToolbar *toolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, [[UIScreen mainScreen] bounds].size.width, 44)];
        [toolbar setItems:[NSArray arrayWithObjects:flexableItem,doneItem, nil]];
        self.inputAccessoryView = toolbar;

    }
    
    [super setKeyboardType:keyboardType];
}
- (void)doneButtonDidPressed:(id)sender {
    [self resignFirstResponder];
}
-(NSString *)checkedText{
    
    if ([self.text isEqualToString:self.placeholderString]) {
        return nil;
    }
    return self.text;
    
}
-(void)setVvTextFieldDelegate:(id<VVTextFieldProtocol>)vvTextFieldDelegate{
    
    
    if ([vvTextFieldDelegate isKindOfClass:[UIViewController class]]) {
        RDViewController *  vc=(RDViewController*)vvTextFieldDelegate;
        
        [vc vvTextFieldAdded];
    }
    _vvTextFieldDelegate=vvTextFieldDelegate;
    
    
    
    
    
}
//







@end
