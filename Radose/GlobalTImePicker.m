//
//  GlobalTImePicker.m
//  TutoringOnDemand
//
//  Created by Aleksandr Padalko on 3/17/14.
//  Copyright (c) 2014 mm7. All rights reserved.
//

#import "GlobalTImePicker.h"
#import "AppDelegate.h"
@interface GlobalTImePicker(){
    UIView * blackView;
    UILabel * selectedLabel;
    NSDateFormatter *  df;
	
	
	NSString * pickedValue;
    
}
@end
@implementation GlobalTImePicker

- (id)init
{
    
    CGRect scr = [UIScreen mainScreen].bounds;
    
    self = [super initWithFrame:CGRectMake(0, 0, scr.size.width, scr.size.height)];

    if (self) {
        pv=[[UIDatePicker alloc]initWithFrame:CGRectMake(0, scr.size.height, self.frame.size.width,scr.size.height/2)];
        
        [pv setFrame:CGRectMake(0, self.frame.size.height, self.frame.size.width, self.frame.size.height)];
       
        [pv setBackgroundColor:[UIColor whiteColor]];
        [self setBackgroundColor:[UIColor  colorWithRed:0 green:0 blue:0 alpha:0.87]];
       // [pv selectRow:0 inComponent:0 animated:NO];
        
        [self addSubview:pv];
        df=[[NSDateFormatter alloc]init];
        [df setDateFormat:@"hh:mma"];
        [pv setDatePickerMode:UIDatePickerModeTime];
     
		[pv  setDate:[NSDate date] animated:YES];
		pickedValue=[df stringFromDate:pv.date];
		
        [pv setMinuteInterval:15];
        
        UIBarButtonItem *doneItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(doneButtonDidPressed:)];
        UIBarButtonItem *flexableItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:NULL];
        UIToolbar *toolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, [[UIScreen mainScreen] bounds].size.width, 44)];
        [toolbar setItems:[NSArray arrayWithObjects:flexableItem,doneItem, nil]];
       // pv.inputAccessoryView = toolbar;
         [self createTopPanel];
        // Initialization code
        
        [pv addTarget:self action:@selector(dateC) forControlEvents:UIControlEventValueChanged];
        
        
        [UIView animateWithDuration:0.33 animations:^{
            [pv setFrame:CGRectMake(0, self.frame.size.height-pv
                                    .frame.size.height, self.frame.size.width, self.frame.size.height)];
        }];
    }
    return self;
}
-(void)dateC{
    
    pickedValue=[df stringFromDate:pv.date];
}
- (void)doneButtonDidPressed:(id)sender {
    [self resignFirstResponder];
}
+(GlobalTImePicker*) createGlobalTimePicker
{
    GlobalTImePicker * d = [[self alloc]init];
	
	AppDelegate *  ap = [UIApplication sharedApplication].delegate;
    [ap.window addSubview:d];
	
	
    return d;
}
-(void) createTopPanel {
	return;
   
    
    
    
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    
    [self.pickerDelegate picked:[df stringFromDate:pv.date]];
    
}
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
	
	UITouch * touch = [touches anyObject];
    CGPoint loc= [touch locationInView:self];
    if (CGRectContainsPoint(pv.frame, loc)) {
		return;
	}
	[self okButPressed];
	

	
}
-(void)okButPressed{
    
    [self.pickerDelegate pickedDate:pv.date];
    pickedValue=[df stringFromDate:pv.date];

            [self.pickerDelegate picked:pickedValue];
    [UIView animateWithDuration:0.33 animations:^{
        [pv setFrame:CGRectMake(0, self.frame.size.height, self.frame.size.width, self.frame.size.height)];
    }completion:^(BOOL fin){

        [self removeFromSuperview];
    
    }];

	// TODO: dealloc

    
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

-(NSDate *)prepareLimitDate:(NSString *)limitDate {
	if (!limitDate) {
		[pv  setDate:[NSDate date] animated:YES];
		pickedValue=[df stringFromDate:pv.date];
		return nil;
	}
	limitDateTime=[df dateFromString:limitDate ];
	
	NSDate *oldDate =[[NSDate alloc]init]; // Or however you get it.
	unsigned unitFlags = NSCalendarUnitYear | NSCalendarUnitMonth |  NSCalendarUnitDay;
	NSCalendar *calendar = [NSCalendar currentCalendar];
	
	NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier: NSGregorianCalendar];
	NSDateComponents *comps = [gregorian components: NSUIntegerMax fromDate: oldDate];
	
	NSDateComponents *comps2 = [gregorian components: NSUIntegerMax fromDate: limitDateTime];
	
	NSLog(@"%d",comps2.hour);
	comps.hour   = comps2.hour;
	comps.minute = comps2.minute;
	comps.second=0;
	NSDate *newDate = [calendar dateFromComponents:comps];
	
	NSLog(@"%@",newDate);
	[pv  setDate:newDate animated:YES];

	return newDate;
}

-(void)setMinDate:(NSString *)minDate {
	NSDate * newDate = [self prepareLimitDate:minDate];
	[pv setMinimumDate:[newDate dateByAddingTimeInterval:15*60]];
	_minDate=minDate;
    	//[pv  setDate:pv.minimumDate animated:YES];
    	}
-(void)setMaxDate:(NSString *)maxDate {
	NSDate * newDate = [self prepareLimitDate:maxDate];
	[pv setMaximumDate:[newDate dateByAddingTimeInterval:-15*60]];
	_maxDate=maxDate;
}

@end
