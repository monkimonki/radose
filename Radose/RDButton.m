//
//  RDButton.m
//  Radose
//
//  Created by mm7 on 18.07.14.
//  Copyright (c) 2014 mm7. All rights reserved.
//

#import "RDButton.h"

@implementation RDButton
+(RDButton *)createWithTitle:(NSString *)title{
    
    RDButton * but=[RDButton buttonWithType:UIButtonTypeCustom];
    
    [but setFrame:CGRectMake(0, 0, 257, 37)];
    [but.titleLabel setFont:[UIFont RDFontMedium]];
    [but setTitleColor:[UIColor colorWithRed:1 green:1 blue:1 alpha:1] forState:UIControlStateNormal];
        [but setTitleColor:[UIColor colorWithRed:1 green:1 blue:1 alpha:0.5] forState:UIControlStateHighlighted];
    but.layer.cornerRadius=2.0;
    [but setTitle:title forState:UIControlStateNormal];
    [but setWhite];
    
    return but;
}
-(void)setWhite{
    [self setBackgroundColor:[UIColor clearColor]];
    
    self.layer.borderColor=[UIColor whiteColor].CGColor;
    self.layer.borderWidth=1.0;
  
}
-(void)setGreen{
    self.layer.borderWidth=0.0;
    self.layer.borderColor=[UIColor clearColor].CGColor;
    [self setBackgroundColor:[UIColor RDGreenColor]];
}
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
