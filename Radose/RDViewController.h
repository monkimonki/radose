//
//  RDViewController.h
//  Radose
//
//  Created by mm7 on 17.07.14.
//  Copyright (c) 2014 mm7. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RDViewController : UIViewController
{
        NSMutableArray * alphaShowArr;
}

-(void)timeAnimation;
@property (retain,nonatomic)NSMutableArray * textFieldsArray;
-(void)addAlphaShowView:(UIView*)v;

-(void)didEndAlphaShow;
-(void)showAlphaArr:(void(^)())block;

-(void)createRightBackButton;
-(void)createLeftBackButton;
@end
