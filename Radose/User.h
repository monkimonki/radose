//
//  User.h
//  Radose
//
//  Created by mm7 on 18.07.14.
//  Copyright (c) 2014 mm7. All rights reserved.
//

#import "RDObject.h"

@interface User : RDObject
@property (retain,nonatomic)NSString * fullname;
@property (retain,nonatomic)NSString * password;
@property (retain,nonatomic)NSString * email;
+(User*)loadUser;
-(void)saveUser;
@end
