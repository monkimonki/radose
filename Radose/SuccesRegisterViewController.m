//
//  SuccesRegisterViewController.m
//  Radose
//
//  Created by mm7 on 18.07.14.
//  Copyright (c) 2014 mm7. All rights reserved.
//

#import "SuccesRegisterViewController.h"
#import "RDButton.h"
#import "RDTabBarViewController.h"
@interface SuccesRegisterViewController ()

@end

@implementation SuccesRegisterViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    UILabel * label=[[UILabel alloc]initWithFrame:CGRectMake(15, self.view.frame.size.height/2-40, self.view.frame.size.width-30, 40)];
    [label setFont:[UIFont RDFontBig]];
    [label setTextColor:[UIColor whiteColor]];
    [label setText:@"Congratulation!"];
       [label setTextAlignment:NSTextAlignmentCenter];
    [self.view addSubview:label];
    
    UILabel * lableDescr=[[UILabel alloc]initWithFrame:CGRectMake(15, label.frame.origin.y+label.frame.size.height, self.view.frame.size.width-30, 30)];
    
    [lableDescr setFont:[UIFont RDFontMedium]];
    [lableDescr setTextAlignment:NSTextAlignmentCenter];
    [lableDescr setTextColor:[UIColor whiteColor]];
    [self.view addSubview:lableDescr];
    [lableDescr setText:[NSString stringWithFormat:@"%@",@"You are succesfull registerd on service\nNow, time for Radose!"]];
    [lableDescr setNumberOfLines:2];
    [alphaShowArr addObject:label];
    [alphaShowArr addObject:lableDescr];
    
    
    RDButton * but=[RDButton createWithTitle:@"Get start"];
    [but setCenter:CGPointMake(self.view.frame.size.width/2, [lableDescr getLastY]+15+but.frame.size.height/2)];
    [but setGreen];
    [but setAlpha:0];
    [self.view addSubview:but];
    [but addTarget:self action:@selector(getStart) forControlEvents:UIControlEventTouchUpInside];
    [self showAlphaArr:^{
        [UIView animateWithDuration:0.33 animations:^{
            [but setAlpha:1];
        }];
    }];
    
    
    
    // Do any additional setup after loading the view.
}
-(void)getStart{
    RDTabBarViewController *t=[[RDTabBarViewController alloc] init];
    
    [self.navigationController setViewControllers:[NSArray arrayWithObject:t] animated:YES];
    
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
