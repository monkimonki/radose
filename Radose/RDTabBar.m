//
//  RDTabBar.m
//  Radose
//
//  Created by mm7 on 18.07.14.
//  Copyright (c) 2014 mm7. All rights reserved.
//

#import "RDTabBar.h"
@interface RDTabBarButton:UIButton{
    UIImageView * imgv;
    
    UILabel * labName;
    
    UIImage * hImage;
    UIImage * uImage;
}
@end

@implementation RDTabBarButton

+(RDTabBarButton*)createWithName:(NSString*)s andFrame:(CGRect)f{
    
    RDTabBarButton * r=[RDTabBarButton buttonWithType:UIButtonTypeCustom];
    [r setFrame:f];
    
    
    [r setup:s];
    
    
    return r;
    
}
-(void)setup:(NSString*)s{
    imgv=[[UIImageView alloc]initWithFrame:CGRectMake(0, 7,self.frame.size.width, 38)];
    [imgv setContentMode:UIViewContentModeCenter];
    [imgv setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@.png",s]]];
    hImage=[UIImage imageNamed:[NSString stringWithFormat:@"%@-s.png",s]];
    uImage=[UIImage imageNamed:[NSString stringWithFormat:@"%@.png",s]];
      [imgv setImage:uImage];
    [self addSubview:imgv];
    
    labName=[[UILabel alloc]initWithFrame:CGRectMake(2, [imgv getLastY]+2, self.frame.size.width-4, 18)];
    [labName setFont:[UIFont RDFontSmall]];
    [labName setTextAlignment:NSTextAlignmentCenter];
    [labName setText:s];
    [labName setBackgroundColor:[UIColor clearColor]];
    [self addSubview:labName];
    [labName setTextColor:[UIColor whiteColor]];
}
-(void)setHighlighted:(BOOL)highlighted{
    if (self.selected) {
        return;
    }
    if (highlighted) {
        [imgv setImage:hImage];
        [labName setTextColor:[UIColor RDOrangeColor]];
    }else{
        
        [imgv setImage:uImage];
        [labName setTextColor:[UIColor whiteColor]];
    }
}
-(void)setSelected:(BOOL)selected{
      [super setSelected:selected];
    if (selected) {
        [imgv setImage:hImage];
        [labName setTextColor:[UIColor RDOrangeColor]];
    }else{
        [imgv setImage:uImage];
        [labName setTextColor:[UIColor whiteColor]];
    }
  
}
@end
@interface RDTabBar(){
    NSMutableArray * buttonsArray;
}

@end
@implementation RDTabBar
+(RDTabBar *)createBar{
    
    RDTabBar *r=[[RDTabBar alloc]initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 77)];
    
    
    
    return r;
    
}
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self createButtonsAndLabels];
        
        [self setBackgroundColor:[UIColor colorWithRed:0 green:0 blue:0 alpha:0.7]];
    }
    return self;
}
-(void)createButtonsAndLabels{
        NSMutableArray * arr=[[NSMutableArray alloc]initWithObjects:locKeyAbout,locKeyDPF,locKeyStat,locKeyAccount, nil];
    float singleButtonSize=self.frame.size.width/arr.count;

      buttonsArray=[[NSMutableArray alloc]init];
    for (int a=0; a<arr.count; a++) {
        NSString * name=[arr objectAtIndex:a];
        RDTabBarButton * r=[RDTabBarButton createWithName:name andFrame:CGRectMake(singleButtonSize*a, 0, singleButtonSize, self.frame.size.height)];
        [self addSubview:r];
        [r setTag:a];
        [r addTarget:self action:@selector(didSelect:) forControlEvents:UIControlEventTouchUpInside];
        
        [buttonsArray addObject:r];
    }
    
}
-(void)setSelectedIndex:(int)selectedIndex{
    _selectedIndex=selectedIndex;
    for (UIButton * but1 in buttonsArray) {
        if (selectedIndex!=but1.tag) {
            [but1 setSelected:NO];
        }else{
            [but1 setSelected:YES];
        }
    }

    [self.delegate rdTabBar:self didSelectButtonAtIndex:selectedIndex];
}
-(void)didSelect:(UIButton*)but{
    
    [self setSelectedIndex:but.tag];
    
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
