//
//  AAPMutableRequest.h
//  Chooose
//
//  Created by mm7 on 01.02.14.
//  Copyright (c) 2014 mm7. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ServerMethods.h"
enum kMethodTypes{
    
    kMethodTypeGet=1,
    kMethodTypePut,
    KMethodTypePost,
    kMethodTypeDelete
    
};
@interface AAPMutableRequest : NSMutableURLRequest

@property(nonatomic,retain)NSString * methodName;

-(id)initWithMethodName:(NSString*)mName andMethodType:(int)methodType andParams:(NSArray*)par;

-(void)setBodyAsDictionary:(NSDictionary*)dic;
@property(nonatomic)int method;
-(id)initWithString:(NSString*)str;
@end
