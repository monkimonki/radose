//
//  SelectDateViewController.h
//  Radose
//
//  Created by mm7 on 19.07.14.
//  Copyright (c) 2014 mm7. All rights reserved.
//

#import "RDViewController.h"
@protocol SelectDateDelgate<NSObject>
-(void)didSelectDate:(NSDate*)date;

@end

@interface SelectDateViewController : RDViewController

@property (assign,nonatomic)id<SelectDateDelgate> delegate;
@end
