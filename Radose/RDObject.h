//
//  RDObject.h
//  Radose
//
//  Created by mm7 on 18.07.14.
//  Copyright (c) 2014 mm7. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RDObject : NSObject
@property (retain,nonatomic)NSString * objectId;
-(NSString*)getReqType;
@end
