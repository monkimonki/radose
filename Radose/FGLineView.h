//
//  LineView.h
//  tutoringondemand
//
//  Created by mm7 on 27.03.14.
//  Copyright (c) 2014 mm7. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FGLineView : UIView
@property (retain,nonatomic)UIColor * lineColor;
@end
