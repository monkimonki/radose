//
//  SelectAirPortViewController.h
//  Radose
//
//  Created by mm7 on 19.07.14.
//  Copyright (c) 2014 mm7. All rights reserved.
//

#import "RDViewController.h"
@protocol SelectAirportDelegate<NSObject>
-(void)didSelectAirpord:(NSDictionary*)d;

@end
@interface SelectAirPortViewController : RDViewController
@property (assign,nonatomic)id<SelectAirportDelegate>delegate;
@end
