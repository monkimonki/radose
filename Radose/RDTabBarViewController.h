//
//  RDTabBarViewController.h
//  Radose
//
//  Created by mm7 on 17.07.14.
//  Copyright (c) 2014 mm7. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RDTabBarViewController : UITabBarController
-(void)tabbarTimeAnimation;

-(void)selectIndex:(int)idx;
@end
