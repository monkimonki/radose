//
//  ProgressView.h
//  Radose
//
//  Created by mm7 on 20.07.14.
//  Copyright (c) 2014 mm7. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol ProgressViewDelegare<NSObject>

-(void)didFinishProgress;
@end
@interface ProgressView : UIView
@property (nonatomic)int percent;

@property (nonatomic)float radiance;

@property (assign,nonatomic)id<ProgressViewDelegare>delegate;
  

@end
