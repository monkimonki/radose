//
//  RDNavigationController.m
//  Radose
//
//  Created by mm7 on 17.07.14.
//  Copyright (c) 2014 mm7. All rights reserved.
//

#import "RDNavigationController.h"
#import "StartViewController.h"
#import "RDTabBarViewController.h"
#import <AVFoundation/AVAudioPlayer.h>
#import <AudioToolbox/AudioToolbox.h>
@interface RDNavigationController ()
{
    AVAudioPlayer *player;
    NSTimer * logoTimerShake;
    AVAudioPlayer * shortEffect;
    BOOL isRoot;
}
@end
static RDNavigationController * instance;
@implementation RDNavigationController
-(id)initStartViewController{
    StartViewController * s=[[StartViewController alloc] init];
    if (self=[self initWithRootViewController:s]) {
        
        
        
        
    }
        instance=self;
    return self;
    
}
-(id)initDefaultViewController{
    RDTabBarViewController * s=[[RDTabBarViewController alloc] init];
    if (self=[self initWithRootViewController:s]) {
        
        
        
        
    }
        instance=self;
    return self;
}

-(id)initSubControllerWithRoot:(id)vc{
    if (self=[self initWithRootViewController:vc]) {
        
        
        isRoot=NO;
        
    }
    return self;
}
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(id)initWithRootViewController:(UIViewController *)rootViewController{
    
    if (self==[super initWithRootViewController:rootViewController]) {

        isRoot=YES;
    }
    

    return self;
    
}
+(RDNavigationController*)sharedInstance{
    
    
    return instance;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if (isRoot) {
        
   
    NSString *soundFilePath = [[NSBundle mainBundle] pathForResource:@"2" ofType:@"wav"];
    NSURL *soundFileURL = [NSURL fileURLWithPath:soundFilePath];
//    player = [[AVAudioPlayer alloc] initWithContentsOfURL:soundFileURL error:nil];
//    player.numberOfLoops = 2400; //infinite
//    [player play];
    
    
    
    soundFilePath = [[NSBundle mainBundle] pathForResource:@"5" ofType:@"wav"];
    soundFileURL = [NSURL fileURLWithPath:soundFilePath];
    shortEffect = [[AVAudioPlayer alloc] initWithContentsOfURL:soundFileURL error:nil];
    shortEffect.numberOfLoops = 0; //infinite
  


    [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"bg"]]];
    }
       [self setNavigationBarHidden:YES];
    //  [[UIApplication sharedApplication]setStatusBarStyle:UIStatusBarStyleDefault];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - logo animation
-(void)showLogoWithAnimation:(BOOL)animation andCompl:(void (^)())block{
    if (_logoImageView||_logoImageView.alpha==1) {
        return;
    }
    if (!_logoImageView) {
        _logoImageView=[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"Logo"]];
        [_logoImageView setCenter:CGPointMake(self.view.frame.size.width/2, self.view.frame.size.height/4)];
        
        
        
        
        [self.view addSubview:_logoImageView];
    }
    
    
  
    
    
    
    if (animation) {
          [_logoImageView setAlpha:0];
        [UIView animateWithDuration:1 animations:^{
            [_logoImageView setAlpha:1];
        }completion:^(BOOL finished) {
            block();
        }];
    }else{
        [_logoImageView setAlpha:1];
    }
    
      logoTimerShake =[NSTimer scheduledTimerWithTimeInterval:arc4random()%20+4 target:self selector:@selector(shakeAnimation) userInfo:nil repeats:NO];
    
}
-(void)hideLogoWithAnitmation:(BOOL)animation andCompl:(void (^)())block{
    if (animation) {
        [_logoImageView setAlpha:1];
        [UIView animateWithDuration:0.33 animations:^{
            [_logoImageView setAlpha:0];
        }completion:^(BOOL finished) {
            block();
            [_logoImageView removeFromSuperview];
            _logoImageView=nil;
        }];
    }else{
    
        [_logoImageView setAlpha:0];
        [_logoImageView removeFromSuperview];
        _logoImageView=nil;
    }
    
    [logoTimerShake invalidate];
    logoTimerShake=nil;
}

-(void)shakeAnimation{
        if (_logoImageView&&_logoImageView.alpha==1) {
    CABasicAnimation *animation =
    [CABasicAnimation animationWithKeyPath:@"position"];
    [animation setDuration:0.02];
    [animation setRepeatCount:4];
    [animation setAutoreverses:YES];
    [animation setFromValue:[NSValue valueWithCGPoint:
                             CGPointMake([_logoImageView center].x -2, [_logoImageView center].y)]];
    [animation setToValue:[NSValue valueWithCGPoint:
                           CGPointMake([_logoImageView center].x +2, [_logoImageView center].y)]];
    [[_logoImageView layer] addAnimation:animation forKey:@"position"];
    
      [shortEffect play];
        if ([self.visibleViewController isKindOfClass:[RDViewController class]]) {
            [self.visibleViewController performSelector:@selector(timeAnimation) withObject:nil afterDelay:0.02*5];
        }else if ([self.visibleViewController isKindOfClass:[RDTabBarViewController class]]){
                      [self.visibleViewController performSelector:@selector(tabbarTimeAnimation) withObject:nil afterDelay:0.02*5];
        }

            logoTimerShake =[NSTimer scheduledTimerWithTimeInterval:arc4random()%10+4 target:self selector:@selector(shakeAnimation) userInfo:nil repeats:NO];
          
    }
    
}

-(void)timeAnimation{
    if ([self.visibleViewController isKindOfClass:[RDViewController class]]) {
        [self.visibleViewController performSelector:@selector(timeAnimation) withObject:nil afterDelay:0.02*5];
    }else if ([self.visibleViewController isKindOfClass:[RDTabBarViewController class]]){
        [self.visibleViewController performSelector:@selector(tabbarTimeAnimation) withObject:nil afterDelay:0.02*5];
    }
}
-(UIViewController *)popViewControllerAnimated:(BOOL)animated{
    [UIView animateWithDuration:0.33 animations:^{
        [self.visibleViewController.view setAlpha:0];
        //  [viewController.view setAlpha:1];
        
    }completion:^(BOOL finished) {
   
    }];

        return    [super popViewControllerAnimated:  NO];
}
-(void)pushViewController:(UIViewController *)viewController animated:(BOOL)animated{
   // [viewController.view setAlpha:0];
//[self.view addSubview:viewController.view];
    [UIView animateWithDuration:0.33 animations:^{
        [self.visibleViewController.view setAlpha:0];
      //  [viewController.view setAlpha:1];

    }completion:^(BOOL finished) {
        [self.visibleViewController.view setAlpha:1];
                [super pushViewController:viewController animated:NO];
    }];

    
}
-(void)setViewControllers:(NSArray *)viewControllers animated:(BOOL)animated{
    
    [UIView animateWithDuration:0.33 animations:^{
        [self.visibleViewController.view setAlpha:0];
        //  [viewController.view setAlpha:1];
        
    }completion:^(BOOL finished) {
        [self.visibleViewController.view setAlpha:1];
        [super setViewControllers:viewControllers animated:NO];
    }];
    
}
#pragma mark -end
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
