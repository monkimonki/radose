//
//  TotalCalculation.h
//  Radose
//
//  Created by mm7 on 30.07.14.
//  Copyright (c) 2014 mm7. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TotalCalculation : NSObject
@property (retain,nonatomic) NSNumber * totalFlights;
@property (retain,nonatomic) NSNumber * totalDose;
@end
