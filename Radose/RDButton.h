//
//  RDButton.h
//  Radose
//
//  Created by mm7 on 18.07.14.
//  Copyright (c) 2014 mm7. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RDButton : UIButton
+(RDButton*)createWithTitle:(NSString*)title;
-(void)setWhite;
-(void)setGreen;
@end
