//
//  RDViewController.m
//  Radose
//
//  Created by mm7 on 17.07.14.
//  Copyright (c) 2014 mm7. All rights reserved.
//

#import "RDViewController.h"

@interface RDViewController (){

}

@end

@implementation RDViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
//    self.navigationController.interactivePopGestureRecognizer.delegate = self;
    alphaShowArr=[[NSMutableArray alloc]init];
    _textFieldsArray=[[NSMutableArray alloc]init];
    [self.view setBackgroundColor:[UIColor clearColor]];
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)timeAnimation{
    
    for (int a=0; a<alphaShowArr.count; a++) {
        
        [UIView animateWithDuration:(arc4random()%10+10)/100.0 delay:a*(arc4random()%15)/100.0  options:UIViewAnimationOptionCurveEaseInOut animations:^{
            UIView * v=[alphaShowArr objectAtIndex:a];
            
            
            float alp=(arc4random()%3+2)/10.0;
            NSLog(@"%f",alp);
            [v setAlpha:alp];
        } completion:^(BOOL finished) {
            UIView * v=[alphaShowArr objectAtIndex:a];
            [v setAlpha:1];
        }];
        
    }
    
}
-(void)createLeftBackButton{
    
    UIButton * but=[UIButton buttonWithType:UIButtonTypeCustom];
    [but setFrame:CGRectMake(15, 20, 46, 46)];
    [but setBackgroundColor:[UIColor whiteColor]];
    [but addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:but];
}
-(void)back{
    
    [self.navigationController popToRootViewControllerAnimated:NO];
    
}
-(void)showAlphaArr:(void (^)())block{
    
    for (int a=0; a<alphaShowArr.count; a++) {
        [UIView animateWithDuration:0.33 delay:a*0.2 options:UIViewAnimationOptionCurveEaseInOut animations:^{
            UIView * v=[alphaShowArr objectAtIndex:a];
            [v setAlpha:1];
        } completion:^(BOOL finished) {
            if (a==alphaShowArr.count-1) {
                block();
            }
        }];
    }
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
-(void)addAlphaShowView:(UIView *)v{
    if (!alphaShowArr) {
        alphaShowArr=[[NSMutableArray alloc]init];
    }
    [v setAlpha:0];
    [alphaShowArr addObject:v];
    
}
@end
