//
//  RDNavigationController.h
//  Radose
//
//  Created by mm7 on 17.07.14.
//  Copyright (c) 2014 mm7. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RDNavigationController : UINavigationController

@property (retain,nonatomic) UIImageView * logoImageView;







+(RDNavigationController*)sharedInstance;

-(id)initStartViewController;



-(id)initDefaultViewController;

-(id)initSubControllerWithRoot:(id)vc;
-(void)timeAnimation;


-(void)showLogoWithAnimation:(BOOL)animation andCompl:(void(^)())block;
-(void)hideLogoWithAnitmation:(BOOL)animation andCompl:(void(^)())block;
@end
