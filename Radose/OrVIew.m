//
//  OrVIew.m
//  Radose
//
//  Created by mm7 on 18.07.14.
//  Copyright (c) 2014 mm7. All rights reserved.
//

#import "OrVIew.h"

@implementation OrVIew

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
     //   [self anim];
        
        self.transform=CGAffineTransformMakeRotation(M_PI+M_PI/2);
      
       
    }
    return self;
}
-(void)anim{
    
    UILabel * orlab=[[UILabel alloc]initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
    [orlab setFont:[UIFont RDFontSmall]];
    [orlab setTextColor:[UIColor whiteColor]];
    [orlab setTextAlignment:NSTextAlignmentCenter];
    [orlab setText:@"or"];
    orlab.transform=CGAffineTransformMakeRotation(M_PI/2);
    [self addSubview:orlab];
    [orlab setAlpha:0];
    [UIView animateWithDuration:0.6 animations:^{
        [orlab setAlpha:1];
    }];
    
    
     [self drawLine:CGPointMake(self.frame.size.width/2, 0) len:11.5 onView:self];
  
    [self performSelector:@selector(second) withObject:nil afterDelay:0.33];
      [self performSelector:@selector(third) withObject:nil afterDelay:0.25+0.33];
       // [OrVIew drawLine:CGPointMake(11.5+radius*2, self.frame.size.height/2) len:10 onView:self];
}
-(void)third{
    [self drawLine:CGPointMake(self.frame.size.width/2, self.frame.size.height/2+11) len:11.5 onView:self];
}
-(void)second{
    int radius = 11;
    CAShapeLayer *circle = [CAShapeLayer layer];
    // Make a circular shape
    circle.path = [UIBezierPath bezierPathWithRoundedRect:CGRectMake(0, 0, 2.0*radius, 2.0*radius)
                                             cornerRadius:radius].CGPath;
    // Center the shape in self.view
    circle.position = CGPointMake(self.frame.size.width/2-radius,self.frame.size.height/2-radius);
    
    // Configure the apperence of the circle
    circle.fillColor = [UIColor clearColor].CGColor;
    circle.strokeColor = [UIColor whiteColor].CGColor;
    circle.lineWidth = 1;
    //    CATransform3D transform = CATransform3DIdentity;
    //    transform = CATransform3DTranslate(transform, self.frame.size.width/2- circle.position.x, self.frame.size.height/2- circle.position.y, 0.0);
    //    transform = CATransform3DRotate(transform,0.5, 0.0, 0.0, -1.0);
    //    transform = CATransform3DTranslate(transform,  circle.position.x-self.frame.size.width/2, circle.position.y-self.frame.size.height/2, 0.0);
    // Add to parent layer
    [self.layer addSublayer:circle];
    
    // Configure animation
    CABasicAnimation *drawAnimation = [CABasicAnimation animationWithKeyPath:@"strokeEnd"];
    drawAnimation.duration            = 0.5; // "animate over 10 seconds or so.."
    drawAnimation.repeatCount         = 1.0;  // Animate only once..
    
    // Animate from no part of the stroke being drawn to the entire stroke being drawn
    drawAnimation.fromValue = [NSNumber numberWithFloat:0.0];
    drawAnimation.toValue   = [NSNumber numberWithFloat:1.0f];
    
    
    // Experiment with timing to get the appearence to look the way you want
    drawAnimation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear];
    
    // Add the animation to the circle
    [circle addAnimation:drawAnimation forKey:@"drawCircleAnimation"];
}
-(CAShapeLayer*)drawLine:(CGPoint)pos len:(float)lenght onView:(UIView*)v{
    
    UIBezierPath *path = [UIBezierPath bezierPath];
    [path moveToPoint:pos];
    
    [path addLineToPoint:CGPointMake(pos.x, pos.y+lenght)];
    
    
    
    
    CAShapeLayer *pathLayer = [CAShapeLayer layer];
    pathLayer.frame = v.bounds;
    pathLayer.path = path.CGPath;
    pathLayer.strokeColor = [[UIColor whiteColor] CGColor];
    pathLayer.fillColor = nil;
    pathLayer.lineWidth = 1.0f;
    pathLayer.lineJoin = kCALineJoinBevel;
    
    [v.layer addSublayer:pathLayer];
    
    CABasicAnimation *pathAnimation = [CABasicAnimation animationWithKeyPath:@"strokeEnd"];
    pathAnimation.duration = 0.33;
        pathAnimation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear];
    pathAnimation.fromValue = [NSNumber numberWithFloat:0.0f];
    pathAnimation.toValue = [NSNumber numberWithFloat:1.0f];
    [pathLayer addAnimation:pathAnimation forKey:@"strokeEnd"];
    
    
    
    return pathLayer;
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
