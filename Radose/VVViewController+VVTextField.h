//
//  UIViewController+VVTextField.h
//  FGApp
//
//  Created by mm7 on 10.05.14.
//  Copyright (c) 2014 mm7. All rights reserved.
//

#import "RDViewController.h"
#import "VVTextFieldHelper.h"
@interface RDViewController (VVTextField)<VVTextFieldProtocol,UITextViewDelegate>

-(void)vvTextFieldAdded;

-(UIView*) curResponder;


@end
