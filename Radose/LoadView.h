//
//  LoadView.h
//  Radose
//
//  Created by mm7 on 21.07.14.
//  Copyright (c) 2014 mm7. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoadView : UIView
-(void)appear;
+(LoadView*)createWithName:(NSString*)name;
-(void)dissapper;
@end
