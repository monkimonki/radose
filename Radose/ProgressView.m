//
//  ProgressView.m
//  Radose
//
//  Created by mm7 on 20.07.14.
//  Copyright (c) 2014 mm7. All rights reserved.
//

#import "ProgressView.h"
#import <AVFoundation/AVAudioPlayer.h>
#import <AudioToolbox/AudioToolbox.h>

@interface ProgressView(){
    AVAudioPlayer  *shortEffect;
    int totalSticks;
    
    CGSize stickSize;
    float stickSpace;
    NSTimer * stopTimer;
    
      SystemSoundID mySound;
    

}
@end
@implementation ProgressView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        NSString *soundFilePath = [[NSBundle mainBundle] pathForResource:@"click" ofType:@"mp3"];
        NSURL *soundFileURL = [NSURL fileURLWithPath:soundFilePath];
        shortEffect = [[AVAudioPlayer alloc] initWithContentsOfURL:soundFileURL error:nil];
        
         AudioServicesCreateSystemSoundID((__bridge CFURLRef)(soundFileURL), &mySound);
//        shortEffect.numberOfLoops = 0; //infinite
//        shortEffect.currentTime=0;
//        [shortEffect play];
        
        stickSize =CGSizeMake(2, 18);
        stickSpace=3;
        
        totalSticks = (self.frame.size.width+stickSpace)/(stickSize.width + stickSpace);
        [self setBackgroundColor:[UIColor clearColor]];
        
        
        [self setClipsToBounds:NO];
        
        {
        UILabel * min=[[UILabel alloc]initWithFrame:CGRectMake(0, 0, 100, self.frame.size.height-stickSize.height-5)];
    
        [min setFont:[UIFont RDFontSmall]];
        [min setTextColor:[UIColor whiteColor]];
        [min setText:@"0 %"];
        [min setTextAlignment:NSTextAlignmentCenter];
        [min sizeToFit];
        
        
        [self addSubview:min];
        
        
        [min setCenter:CGPointMake(0, min.center.y)];
        
        }
        
        
        {
            UILabel * min=[[UILabel alloc]initWithFrame:CGRectMake(0, 0, 100, self.frame.size.height-stickSize.height-5)];
            
            [min setFont:[UIFont RDFontSmall]];
        [min setTextColor:[UIColor whiteColor]];
            [min setText:@"100 %"];
            [min setTextAlignment:NSTextAlignmentCenter];
            [min sizeToFit];
            
            
            [self addSubview:min];
            
            
            [min setCenter:CGPointMake(self.frame.size.width/2, min.center.y)];
            
        }
        
        {
            UILabel * min=[[UILabel alloc]initWithFrame:CGRectMake(0, 0, 100, self.frame.size.height-stickSize.height-5)];
            
            [min setFont:[UIFont RDFontSmall]];
               [min setTextColor:[UIColor whiteColor]];
            [min setText:@"300 %"];
            [min setTextAlignment:NSTextAlignmentCenter];
            [min sizeToFit];
            
            
            [self addSubview:min];
            
            
            [min setCenter:CGPointMake(self.frame.size.width, min.center.y)];
            
        }
        
        
        
    }
    return self;
}

-(void)setPercent:(int)percent{
    _percent=percent;
 
     [self setNeedsDisplay];
    
    SystemSoundID mySound1;
    NSString *soundFilePath = [[NSBundle mainBundle] pathForResource:@"click" ofType:@"mp3"];
    NSURL *soundFileURL = [NSURL fileURLWithPath:soundFilePath];
    AudioServicesCreateSystemSoundID((__bridge CFURLRef)(soundFileURL), &mySound1);
    AudioServicesPlaySystemSound(mySound1);

}
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
   
    int val= (totalSticks/100.0)*_percent;
    float p=0;
     float d=0;
    int  k=0;
    for (int a=0; a< totalSticks; a++) {
        
        
        CGRect rectangle = CGRectMake(0+(stickSpace+stickSize.width)*a, rect.size.height-stickSize.height, stickSize.width, stickSize.height);
        CGContextRef context = UIGraphicsGetCurrentContext();
        if (a<=val) {
            if (p<=0.5) {
                CGContextSetRGBFillColor(context, 0.0 +p*2, 1.0, 0.0, 1.0);
                CGContextSetRGBStrokeColor(context, 0.0, 0.0, 0.0, 0.0);
                k=a;
            }else{
                
                d+=(100.0/k)/100.0;
                CGContextSetRGBFillColor(context, 1.0 , 1.0-d, 0.0, 1.0);
                CGContextSetRGBStrokeColor(context, 0.0, 0.0, 0.0, 0.0);
            //    NSLog(@"D: %f",d);
            }
      
        }else{
        CGContextSetRGBFillColor(context,161.0/255.0, 160.0/255.0, 161.0/255.0, 1.0);
        CGContextSetRGBStrokeColor(context, 0.0, 0.0, 0.0, 0.0);
        }
        CGContextFillRect(context, rectangle);
        
        
        p+=(100.0/totalSticks)/100.0;
       // NSLog(@"P: %f",p);
    }
    

}
-(void)setP:(NSNumber*)n{

    [self setPercent:[n intValue]];
   
}
-(void)setRadiance:(float)radiance{
    int r=(radiance/10/3)*100;
    int p=arc4random() %50+50;
    float t=0;

    
    for (int a=0; a<=p; a++) {
       
        [self performSelector:@selector(setP:) withObject:[NSNumber numberWithInt:a] afterDelay:t];
         t+=0.01;
    }
    
    
    for (int a=p; a>=0; a--) {
        t+=0.008;
     [self performSelector:@selector(setP:) withObject:[NSNumber numberWithInt:a] afterDelay:t];
    }

    for (int a=0; a<=r; a++) {
        if (a<r-3) {
            [self performSelector:@selector(setP:) withObject:[NSNumber numberWithInt:a] afterDelay:t];
            t+=0.016;
        }else{
             t+=0.1;
        [self performSelector:@selector(setP:) withObject:[NSNumber numberWithInt:a] afterDelay:t];
       
        }
    }
    
    [self performSelector:@selector(fin) withObject:nil afterDelay:t];
}
-(void)fin{
    [self.delegate didFinishProgress];
}
- (void)playAtTime:(float)time withDuration:(NSTimeInterval)duration {
    NSTimeInterval shortStartDelay = time;
    NSTimeInterval now = shortEffect.deviceCurrentTime;
    
    [shortEffect playAtTime:now + shortStartDelay];
    stopTimer = [NSTimer scheduledTimerWithTimeInterval:shortStartDelay + duration
                                                      target:self
                                                    selector:@selector(stopPlaying:)
                                                    userInfo:nil
                                                     repeats:NO];
}

- (void)stopPlaying:(NSTimer *)theTimer {
    [shortEffect pause];
}

@end
