//
//  RegisterViewController.m
//  Radose
//
//  Created by mm7 on 18.07.14.
//  Copyright (c) 2014 mm7. All rights reserved.
//

#import "RegisterViewController.h"
#import "RDTextField.h"
  #import <MobileCoreServices/UTCoreTypes.h>
#import "RDButton.h"
#import "SuccesRegisterViewController.h"
@interface RegisterViewController ()<UIActionSheetDelegate ,UIImagePickerControllerDelegate,UINavigationControllerDelegate>{
    RDTextField * loginTextField;
    RDTextField * fullnameTextField;
    RDTextField * passwordTextField;
    RDTextField * repeatPasswordTextField;
    
    
    UIImageView * avaImageview;
    
    UIImage * curImage;
    
}

@end

@implementation RegisterViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self createLeftBackButton];
    loginTextField=[RDTextField createBase];
    
    [loginTextField setCenter:CGPointMake(self.view.frame.size.width/2,self.view.frame.size.height/2-loginTextField.frame.size.height*2)];
    
    [self.view addSubview:loginTextField];
    [self addAlphaShowView:loginTextField];
    [loginTextField addImage:[UIImage imageNamed:@"email_ico"]];
    [loginTextField setPlaceholder:@"email"];
    
    fullnameTextField=[RDTextField createBase];
       [fullnameTextField addImage:[UIImage imageNamed:@"name_ico"]];
    [fullnameTextField setCenter:CGPointMake(self.view.frame.size.width/2,[loginTextField getLastY]+5+fullnameTextField.frame.size.height/2)];
    
    [self.view addSubview:fullnameTextField];
    [self addAlphaShowView:fullnameTextField];
    
     [fullnameTextField setPlaceholder:@"Full Name"];
    
    passwordTextField=[RDTextField createBase];
        [passwordTextField addImage:[UIImage imageNamed:@"lock"]];
    [passwordTextField setCenter:CGPointMake(self.view.frame.size.width/2,[fullnameTextField getLastY]+5+fullnameTextField.frame.size.height/2)];
    [passwordTextField setSecureTextEntry:YES];
    [passwordTextField setPlaceholder:@"password"];
    
    [self.view addSubview:passwordTextField];
    [self addAlphaShowView:passwordTextField];
    
    
    
    repeatPasswordTextField=[RDTextField createBase];
       [repeatPasswordTextField setPlaceholder:@"repeat password"];
            [repeatPasswordTextField addImage:[UIImage imageNamed:@"lock"]];
    [repeatPasswordTextField setCenter:CGPointMake(self.view.frame.size.width/2,[passwordTextField getLastY]+5+fullnameTextField.frame.size.height/2)];
        [repeatPasswordTextField setSecureTextEntry:YES];
    [self.view addSubview:repeatPasswordTextField];
    [self addAlphaShowView:repeatPasswordTextField];
    [loginTextField setVvTextFieldDelegate:self];
    [fullnameTextField setVvTextFieldDelegate:self];
    [passwordTextField setVvTextFieldDelegate:self];
    [repeatPasswordTextField setVvTextFieldDelegate:self];
    
  
    UIButton* butGallery=[UIButton buttonWithType:UIButtonTypeCustom];
    [butGallery setFrame:CGRectMake(26, [repeatPasswordTextField getLastY]+37, self.view.frame.size.width/2-26, 40)];
    [butGallery addTarget:self action:@selector(gallery) forControlEvents:UIControlEventTouchUpInside];
    {
        UILabel * lab=[[UILabel alloc]initWithFrame:CGRectMake(butGallery.frame.size.height+5, 0, butGallery.frame.size.width-butGallery.frame.size.height-15, butGallery.frame.size.height)];
        [lab setBackgroundColor:[UIColor clearColor]];
        [lab setFont:[UIFont RDFontMedium]];
        [lab setTextColor:[UIColor whiteColor]];
        [lab setText:@"Gallery"];
        [butGallery addSubview:lab];
        UIImageView *imgv=[[UIImageView alloc]initWithFrame:CGRectMake(5, 0, butGallery.frame.size.height, butGallery.frame.size.height)];
        [imgv setImage:[UIImage imageNamed:@"gallery"]];
        [imgv setContentMode:UIViewContentModeCenter];
        [butGallery addSubview:imgv];
    }
    
    
    [butGallery setAlpha:0];
    [self.view addSubview:butGallery];
    UIButton* butPhoto=[UIButton buttonWithType:UIButtonTypeCustom];
    [butPhoto setFrame:CGRectMake(self.view.frame.size.width-26-(self.view.frame.size.width/2-26), [repeatPasswordTextField getLastY]+37, self.view.frame.size.width/2-26, 40)];
    
    {
        UILabel * lab=[[UILabel alloc]initWithFrame:CGRectMake(40, 0, butGallery.frame.size.width-butGallery.frame.size.height-40-5, butGallery.frame.size.height)];
        [lab setBackgroundColor:[UIColor clearColor]];
        [lab setFont:[UIFont RDFontMedium]];
        [lab setTextColor:[UIColor whiteColor]];
        [lab setText:@"Photo"];
        [butPhoto addSubview:lab];
        [lab setTextAlignment:NSTextAlignmentCenter];
        UIImageView *imgv=[[UIImageView alloc]initWithFrame:CGRectMake(lab.frame.size.width+40, 0, butGallery.frame.size.height, butGallery.frame.size.height)];
        [imgv setImage:[UIImage imageNamed:@"camera"]];
        [imgv setContentMode:UIViewContentModeCenter];
        [butPhoto addSubview:imgv];
    }
    [butPhoto addTarget:self action:@selector(photo) forControlEvents:UIControlEventTouchUpInside];
    [butPhoto setAlpha:0];
    [self.view addSubview:butPhoto];
 
    RDButton * butSignUp=[RDButton createWithTitle:@"Sign Up"];
    [self.view addSubview:butSignUp];
    [butSignUp setCenter:CGPointMake(self.view.frame.size.width/2, self.view.frame.size.height-40-butSignUp.frame.size.height/2)];
    [butSignUp setGreen];
    [butSignUp addTarget:self action:@selector(signUp) forControlEvents:UIControlEventTouchUpInside];
    [butSignUp setAlpha:0];
    
    UIImageView * border=[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"border_Photoload"]];
    [self.view addSubview:border];

    avaImageview=[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"user_photo"]];
    [avaImageview setCenter:CGPointMake(self.view.frame.size.width/2, butGallery.center.y)];
    avaImageview.layer.cornerRadius=avaImageview.frame.size.height/2;
    [avaImageview setClipsToBounds:YES];
    [self.view addSubview:avaImageview];
    [avaImageview setAlpha:0];
    [border setAlpha:0];
    [avaImageview setContentMode:UIViewContentModeScaleAspectFill];
        [border setCenter:CGPointMake(self.view.frame.size.width/2, avaImageview.center.y)];
    
    
    
    
    
    
    
    
    
    [self showAlphaArr:^{
        
        [UIView animateWithDuration:0.2 animations:^{
            [avaImageview setAlpha:1];
            [border setAlpha:1];
        }];
        [UIView animateWithDuration:0.33 animations:^{
            [butSignUp setAlpha:1];
            [butGallery setAlpha:1];
            [butPhoto setAlpha:1];
        }];
 
    }];
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    
    
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
#pragma mark - func

-(void)signUp{

    
    BOOL first=NO;
    if (loginTextField.text.length==0) {
        [loginTextField setErrorString:@"enter email"];
        first=YES;
    }
    
    
    if (![[RDSingleton sharedInstance] checkEmail:loginTextField.text]) {
        
        [loginTextField setErrorString:@"enter valid email"];
        first=YES;
        
    }
    
    
//    {
//           [loginTextField setErrorString:@"wrong email"];
//    }
    
    
    if (fullnameTextField.text.length==0) {
          first=YES;
        [fullnameTextField setErrorString:@"enter full name"];
    }
    
    if (passwordTextField.text.length==0) {
          first=YES;
        [passwordTextField setErrorString:@"enter password"];
        
    }
    
    if (repeatPasswordTextField.text.length==0 ) {
          first=YES;
        [repeatPasswordTextField setErrorString:@"repeat password"];
    }
    if (  first==YES) {
        return;
    }
    if (![repeatPasswordTextField.text isEqualToString: passwordTextField.text]) {
        [passwordTextField setErrorString:@"passwords not match"] ;
         first=YES;
    }
    
        if (first)
         return;
    
    User * user=[[User alloc]init];
    
    user.email=loginTextField.text;
    user.password=passwordTextField.text;
    user.fullname=fullnameTextField.text;
    
    [[RDSingleton sharedInstance] registerNewUser:user andCompBloc:^(BOOL suc,RDErorr * err) {
        
        NSLog(@"%@",err.erorrText);
        if (suc) {
            SuccesRegisterViewController * s=[[SuccesRegisterViewController alloc] init];
            [self.navigationController pushViewController:s animated:NO];
        }
    }];
    
}
-(void)photo{
    [self shouldStartCameraController];
}
-(void)gallery{
    [self shouldStartPhotoLibraryPickerController];
}



- (BOOL)shouldStartCameraController {
    
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera] == NO) {
        return NO;
    }
    
    UIImagePickerController *cameraUI = [[UIImagePickerController alloc] init];
    
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]
        && [[UIImagePickerController availableMediaTypesForSourceType:
             UIImagePickerControllerSourceTypeCamera] containsObject:(NSString *)kUTTypeImage]) {
        
        cameraUI.mediaTypes = [NSArray arrayWithObject:(NSString *) kUTTypeImage];
        cameraUI.sourceType = UIImagePickerControllerSourceTypeCamera;
        
        if ([UIImagePickerController isCameraDeviceAvailable:UIImagePickerControllerCameraDeviceRear]) {
            cameraUI.cameraDevice = UIImagePickerControllerCameraDeviceRear;
        } else if ([UIImagePickerController isCameraDeviceAvailable:UIImagePickerControllerCameraDeviceFront]) {
            cameraUI.cameraDevice = UIImagePickerControllerCameraDeviceFront;
        }
        
    } else {
        return NO;
    }
    
    cameraUI.allowsEditing = YES;
    cameraUI.showsCameraControls = YES;
    cameraUI.delegate = self;
    
    [self presentViewController:cameraUI animated:YES completion:nil];
    
    return YES;
}

- (BOOL)shouldStartPhotoLibraryPickerController {
    if (([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary] == NO
         && [UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeSavedPhotosAlbum] == NO)) {
        return NO;
    }
    
    UIImagePickerController *cameraUI = [[UIImagePickerController alloc] init];
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary]
        && [[UIImagePickerController availableMediaTypesForSourceType:UIImagePickerControllerSourceTypePhotoLibrary] containsObject:(NSString *)kUTTypeImage]) {
        
        cameraUI.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        cameraUI.mediaTypes = [NSArray arrayWithObject:(NSString *) kUTTypeImage];
        
    } else if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeSavedPhotosAlbum]
               && [[UIImagePickerController availableMediaTypesForSourceType:UIImagePickerControllerSourceTypeSavedPhotosAlbum] containsObject:(NSString *)kUTTypeImage]) {
        
        cameraUI.sourceType = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
        cameraUI.mediaTypes = [NSArray arrayWithObject:(NSString *) kUTTypeImage];
        
    } else {
        return NO;
    }
    
    cameraUI.allowsEditing = YES;
    
    cameraUI.delegate = self;
    
    [self presentViewController:cameraUI animated:YES completion:nil];
    
    return YES;
}
#pragma mark - UIImagePickerDelegate

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    [self dismissViewControllerAnimated:NO completion:nil];
    
    curImage= [info objectForKey:UIImagePickerControllerEditedImage];
    
    [avaImageview setImage:curImage];
}

@end
