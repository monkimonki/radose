//
//  EKTextfield.h
//  TutoringOnDemand
//
//  Created by mm7 on 13.03.14.
//  Copyright (c) 2014 mm7. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VVTextFieldHelper.h"
#import "VVViewController+VVTextField.h"

@interface VVBaseTextField : UITextField <VVTextFieldHelperDelegate> {
    UIColor *placeholderColor;
       UILabel * staticlabel;
}
//

//
@property  (assign, nonatomic) IBOutlet id <VVTextFieldProtocol> vvTextFieldDelegate;
//

@property(strong,nonatomic)VVTextFieldHelper * tfHelper;
@property (retain, nonatomic) NSString *placeholderString;
@property (retain, nonatomic) UIColor *baseColor;
@property (retain,nonatomic)  NSString * staticText;
@property (retain,nonatomic) UIColor * placeHolderColor;
@property (retain,nonatomic) UIColor * staticTextColor;
@property (nonatomic)BOOL manyTextFields;

@property(nonatomic)float leftOffset;
@property(nonatomic)float rightOffset;

-(NSString*)checkedText;
@end
