//
//  RDSingleton.h
//  Radose
//
//  Created by mm7 on 18.07.14.
//  Copyright (c) 2014 mm7. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "User.h"
#import "RDErorr.h"
#import <CoreData/CoreData.h>
@class Calculation;
enum kHistoryLoadType{
    kHistoryLoadType365,
    kHistoryLoadTypeMonth,
    kHistoryLoadTypeDay,
    kHistoryLoadTypeAll
};

@interface NSString (Loc)
-(NSString*)localized;
@end
@interface RDSingleton : NSObject
+(RDSingleton*)sharedInstance;

-(void)registerNewUser:(User*)user andCompBloc:(void(^)(BOOL suc,RDErorr * err))compBlock;

@property (retain,nonatomic)User * curUser;
@property (retain,nonatomic)NSMutableArray * airportsArray;
-(void)loadAirports:(void(^)(BOOL suc,RDErorr*err))block;

-(void)calculate:(int)ti1 airId1:(NSString*)airId1 ti2:(int)ti2 airId2:(NSString*)airId2   andCompBloc:(void(^)(BOOL suc,NSString* result,RDErorr * err))compBlock;

-(void)loginWithEmail:(NSString*)email andPassword:(NSString*)password andCompBloc:(void (^)(BOOL suc,RDErorr* er))block;
-(BOOL)checkEmail:(NSString*)email;
-(NSString*)formEmail:(NSString*)email;



-(void)loadHistoryOfUserWithType:(enum kHistoryLoadType)type lenght:(int)lenght andOffset:(int)offset andBlock:(void(^)(NSDictionary * result,RDErorr * err))block;


-(void)saveUserCalculation:(Calculation*)calc andBlock:(void(^)(BOOL suc,NSString* calcId,RDErorr*err))block;


@end
