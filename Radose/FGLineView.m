//
//  LineView.m
//  tutoringondemand
//
//  Created by mm7 on 27.03.14.
//  Copyright (c) 2014 mm7. All rights reserved.
//

#import "FGLineView.h"

@implementation FGLineView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        [self setBackgroundColor:[UIColor clearColor]];
          self.lineColor=[UIColor grayColor];
    }
    return self;
}

-(id)initWithCoder:(NSCoder *)aDecoder{
    
    if (self=[super initWithCoder:aDecoder]) {
        [self setBackgroundColor:[UIColor clearColor]];
        self.lineColor=[UIColor grayColor];
    }
    return self;
}
-(void)drawRect:(CGRect)rect{
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetStrokeColorWithColor(context, [self.lineColor CGColor]);
    CGContextSetLineWidth(context, 1.0);
    CGContextMoveToPoint(context, 0, 1);
    CGContextAddLineToPoint(context, rect.size.width, 1);
    CGContextDrawPath(context, kCGPathStroke);

}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
