//
//  RDSingleton.m
//  Radose
//
//  Created by mm7 on 18.07.14.
//  Copyright (c) 2014 mm7. All rights reserved.
//

#import "RDSingleton.h"
#import "AAPMutableRequest.h"
#import "RequestToServer.h"
#import "Airports.h"
#import "LoadView.h"
#import "Calculation.h"
@implementation NSString (Loc)
-(NSString*)localized{
    return NSLocalizedString(self, nil);
}
@end
@implementation RDSingleton

static RDSingleton  * instance;
+(RDSingleton*)sharedInstance{
    if (instance==nil) {
        instance=[[RDSingleton alloc]init];
    }
    
    return instance;
}

-(id)init{
    if (self=[super init]) {
        
        _curUser=[User loadUser];
        
        if (_curUser) {
  
            NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
            NSString *documentsDirectory = paths[0];
            NSString *filePath = [documentsDirectory stringByAppendingPathComponent:@"arr.plist"];

                       _airportsArray=[[NSMutableArray alloc] initWithArray:[[NSUserDefaults standardUserDefaults] valueForKey:@"arr"]];
            if (_airportsArray.count==0) {
                _airportsArray=nil;
            }
            
            NSLog(@"%@",[_airportsArray objectAtIndex:arc4random()%9000]);
            
     
        }
    }
    return self;
}
-(NSString *)formEmail:(NSString *)email{
    
    NSString * s1=[email stringByReplacingOccurrencesOfString:@"@" withString:@"0"];
    s1=[s1 stringByReplacingOccurrencesOfString:@"." withString:@"1"];
    s1=[s1 stringByReplacingOccurrencesOfString:@"_" withString:@"2"];
    s1=[s1 stringByReplacingOccurrencesOfString:@"-" withString:@"3"];
    
    return s1;
}
-(BOOL)checkEmail:(NSString*)email{
    
    
    NSArray * arr=[email componentsSeparatedByString:@"@"];
    
    if (arr.count>1) {
        NSString * str2=[arr lastObject];
        
        NSArray * arr2=[str2 componentsSeparatedByString:@"."];
        if (arr2.count>1) {
            return YES;
        }else{
            return NO;
        }
        
        
        
    }else {
        return NO;
    }
    
}
-(void)registerNewUser:(User *)user andCompBloc:(void (^)(BOOL,RDErorr * err ))compBlock{
    LoadView * lv=[LoadView createWithName:@"Loading"];
    
    NSString * url=@"http://radose.com/index.php?r=user/registration";
    
    AAPMutableRequest * req=[[AAPMutableRequest alloc]initWithString:url];
    
    NSMutableDictionary * dict=[[NSMutableDictionary alloc]init];
    
    
    
    [dict setValue:[self formEmail:user.email] forKey:@"RegistrationForm[username]"];
    [dict setValue:@"//site/index" forKey:@"quickregistration"];
    [dict setValue:user.password forKey:@"RegistrationForm[password]"];
    [dict setValue:user.password forKey:@"RegistrationForm[verifyPassword]"];
    [dict setValue:user.email forKey:@"RegistrationForm[email]"];
    [dict setValue:user.fullname forKey:@"Profile[firstname]"];
    [dict setValue:@"yt9" forKey:@"Registration"];
    
    [req setBodyAsDictionary:dict];
//   quickregistration=%2F%2Fsite%2Findex&RegistrationForm%5Busername%5D=asdsda&RegistrationForm%5Bpassword%5D=1234&RegistrationForm%5BverifyPassword%5D=1234&RegistrationForm%5Bemail%5D=asdasdsda%40das.ru&Profile%5Bfirstname%5D=sadasda&Profile%5Blastname%5D=213&yt9=Registration
//    
//    RegistrationForm%5BverifyPassword%5D=1111&RegistrationForm%5Busername%5D=sadas@ew.ru&Registration=yt9&RegistrationForm%5Bpassword%5D=1111&RegistrationForm%5Bemail%5D=sadas@ew.ru&quickregistration=//site/index&Profile%5Bfirstname%5D=sadasd
    
    
    
   [req setMethod:KMethodTypePost];
   // [req setHTTPBody:[str2 dataUsingEncoding:NSUTF8StringEncoding]];
    [req setValue:@"gzip, deflate" forHTTPHeaderField:@"Accept-Encoding"];
    [req setValue:@"text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8" forHTTPHeaderField:@"Accept"];
        [req setValue:@"Accept-Language" forHTTPHeaderField:@"Accept-Language"];
        [req setValue:@"keep-alive" forHTTPHeaderField:@"Connection"];
        [req setValue:@"gzip, deflate" forHTTPHeaderField:@"Accept-Encoding"];

    
         [req setValue:@"radose.com" forHTTPHeaderField:@"Host"];
    RequestToServer* rqts=[[RequestToServer alloc]initWithReq:req];
    [rqts loadRequest:^(BOOL suc, NSArray *data) {
        [lv dissapper];
        if (suc) {
            NSLog(@"%@",data);
            
       
            
            if ([data valueForKey:@"success"]) {
                user.objectId=@"12";
                [user saveUser];
                _curUser=[User loadUser];
                compBlock(YES,nil);
            }else{
                compBlock (NO,[RDErorr createWithType:kErorrTypeDuplicateEmail]);
            }
        }else {
               compBlock (NO,[RDErorr createWithType:kErorrTypeConnectionErorr]);
        }
    }];
    
    
    
}
-(void)loginWithEmail:(NSString*)email andPassword:(NSString*)password andCompBloc:(void (^)(BOOL suc,RDErorr* er))block{
    
        LoadView * lv=[LoadView createWithName:@"Loading"];
    NSString * url=@"http://radose.com/index.php?r=user/login";
    
    AAPMutableRequest * req=[[AAPMutableRequest alloc]initWithString:url];
    
    NSMutableDictionary * dict=[[NSMutableDictionary alloc]init];
    
    
    [dict setValue:@"//site/index" forKey:@"quicklogin"];
    [dict setValue:@"123456" forKey:@"UserLogin[password]"];
    [dict setValue:@"abc@user.com" forKey:@"UserLogin[username]"];
    
    [req setValue:@"gzip, deflate" forHTTPHeaderField:@"Accept-Encoding"];
    [req setValue:@"ru-RU,ru;q=0.8,en-US;q=0.5,en;q=0.3" forHTTPHeaderField:@"Accept-Language"];
    [req setValue:@"keep-alive" forHTTPHeaderField:@"Connection"];
       [req setValue:@"application/json, text/javascript, */*; q=0.01" forHTTPHeaderField:@"Accept"];
    [req setValue:@"radose.com" forHTTPHeaderField:@"Host"];

    [req setValue:@"application/x-www-form-urlencoded; charset=UTF-8" forHTTPHeaderField:@"Content-Type"];
    [req setValue:@"http://radose.com/index.php?r=site/index" forHTTPHeaderField:@"Referer"];
    [req setValue:@"XMLHttpRequest" forHTTPHeaderField:@"X-Requested-With"];
        [req setValue:@"no-chache" forHTTPHeaderField:@"Pragma"];
    
    //[req setValue:@"PHPSESSID=6tq8tg7tviaq9n0mr3do0t09c6; _ym_visorc_25118864=w" forHTTPHeaderField:@"Cookie"];
    [req setBodyAsDictionary:dict];
    [req setMethod:KMethodTypePost];
    
    RequestToServer* rqts=[[RequestToServer alloc]initWithReq:req];
    [rqts loadRequest:^(BOOL suc, NSArray *data) {
        [lv dissapper];
        if (suc) {
            
            if ([[data valueForKey:@"response"] isEqualToString:@"false"]) {
                   block (NO,[RDErorr createWithType:kErorrTypeWrongEmailOrPassword]);
            }else{
                NSLog(@"%@",data);
                _curUser =[[User alloc]init];
                _curUser.email=email;
                _curUser.password=password;
                _curUser.objectId=@"12";
                [_curUser saveUser];
                block (YES,nil);
            }
       
        }else {
                 block (NO,[RDErorr createWithType:kErorrTypeWrongEmailOrPassword]);
        }
    }];
    
    
}
-(void)loadAirports:(void(^)(BOOL suc,RDErorr*err))block{
    NSString * url=@"http://cosslab.ru/workspace/radose/index.php?r=api/get&fn=gka";
    
    AAPMutableRequest * req=[[AAPMutableRequest alloc]initWithString:url];
    [req setMethod:kMethodTypeGet];
    [req setTimeoutInterval:10];
    RequestToServer* rqts=[[RequestToServer alloc]initWithReq:req];

    [rqts loadRequest:^(BOOL suc, NSArray *data) {
        
        if (suc) {
          //  [[NSUserDefaults standardUserDefaults]setValue:data forKey:@"air"];
        
            _airportsArray=[[NSMutableArray alloc]initWithArray:data];
                     //  NSLog(@"%@",data);
//            NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
//            NSString *documentsDirectory = paths[0];
//            NSString *filePath = [documentsDirectory stringByAppendingPathComponent:@"airports.plist"];
            
            
          
            
//            for (NSDictionary *dict in data) {
//                Airports *failedBankInfo = [NSEntityDescription
//                                            insertNewObjectForEntityForName:@"Airports"
//                                            inManagedObjectContext:context];
//                NSLog(@"%@",data);
//                [failedBankInfo setDict:dict];
//                
//                NSError *error;
//                if (![context save:&error]) {
//                    NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
//                }
//            }
            for (NSMutableDictionary * dict in _airportsArray) {
                [dict setValue:@"" forKey:@"gmtoffset"];
                
                if ([[dict valueForKey:@"city_rus"] isKindOfClass:[NSNull class]]) {
                    [dict setValue:@"" forKey:@"city_rus"];
                }
                
            }
            
        
            [[NSUserDefaults standardUserDefaults]setValue:_airportsArray forKey:@"arr"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            
            block(YES,nil);
        }else{
            block(NO,[RDErorr createWithType:kErorrTypeConnectionErorr]);
        }
    }];
}

-(void)calculate:(int)ti1 airId1:(NSString*)airId1 ti2:(int)ti2 airId2:(NSString*)airId2   andCompBloc:(void(^)(BOOL suc,NSString* result,RDErorr * err))compBlock{
    NSString * url=[NSString stringWithFormat:@"http://cosslab.ru/workspace/radose/index.php?r=api/get&fn=cd&air_1=%@&time_1=%d&air_2=%@&time_2=%d",airId1,ti1,airId2,ti2];
    
    AAPMutableRequest * req=[[AAPMutableRequest alloc]initWithString:url];
    [req setMethod:kMethodTypeGet];
    [req setTimeoutInterval:10];
    RequestToServer* rqts=[[RequestToServer alloc]initWithReq:req];
    
    [rqts loadRequest:^(BOOL suc, NSArray *data) {
        if (suc&&[data valueForKey:@"calc"]) {
            compBlock(YES,[data valueForKey:@"calc"],nil);
        }else{
                 compBlock(NO,nil,[RDErorr createWithType:kErorrTypeConnectionErorr]);
        }
    }];
}

-(void)saveUserCalculation:(Calculation*)calc andBlock:(void(^)(BOOL suc,NSString* calcId,RDErorr*err))block{
    
    LoadView * lv=[LoadView createWithName:@"Loading"];
    NSString * url=@"http://cosslab.ru/workspace/radose/index.php?r=api/set";
    
    AAPMutableRequest * req=[[AAPMutableRequest alloc]initWithString:url];
    
    NSMutableDictionary * dict=[[NSMutableDictionary alloc]init];
    

    [dict setValue:[calc getReqType] forKey:@"fn"];
    [dict setValue:@"1233" forKey:@"users_id"];
    
        [dict setValue:calc.air1 forKey:@"air_1"];
        [dict setValue:[calc.ti1 stringValue] forKey:@"time_1"];
        [dict setValue:calc.air2 forKey:@"air_2"];
        [dict setValue:[calc.ti2 stringValue] forKey:@"time_2"];
    
    
    
    
    [req setValue:@"gzip, deflate" forHTTPHeaderField:@"Accept-Encoding"];
    [req setValue:@"Accept-Language" forHTTPHeaderField:@"Accept-Language"];
    [req setValue:@"keep-alive" forHTTPHeaderField:@"Connection"];
    [req setValue:@"gzip, deflate" forHTTPHeaderField:@"Accept-Encoding"];
    [req setValue:@"radose.com" forHTTPHeaderField:@"Host"];
        [req setValue:@"text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8" forHTTPHeaderField:@"Accept"];
    
    [req setBodyAsDictionary:dict];
    [req setMethod:KMethodTypePost];
    
    RequestToServer* rqts=[[RequestToServer alloc]initWithReq:req];
    [rqts loadRequest:^(BOOL suc, NSArray *data) {
        [lv dissapper];
        if (suc) {
     
            NSLog(@"%@",data);
            
    //        block (YES,nil);
        }else {
      //      block (YES,[RDErorr createWithType:kErorrTypeWrongEmailOrPassword]);
        }
    }];

    
    
}
#pragma mark - Application's Documents directory

// Returns the URL to the application's Documents directory.


@end
