//
//  RequestToServer.h
//  Chooose
//
//  Created by mm7 on 01.02.14.
//  Copyright (c) 2014 mm7. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AAPMutableRequest.h"


typedef void(^bBlock)(BOOL suc,NSArray* data);
@protocol RequestToServerDelegate<NSObject>
@optional
-(void)sucess:(NSArray*)response;
-(void)failed;

@end

@interface RequestToServer : NSObject<NSURLConnectionDelegate>{
    AAPMutableRequest * request;
    
    NSMutableData * requestData;
    int errorCode;
}
@property(assign,nonatomic)id<RequestToServerDelegate> requestToServerDelegate;
-(id)initWithReq:(AAPMutableRequest*)request;
-(void)setRequestBodyData:(id)data;
-(void)loadRequest;
@property (readwrite,copy)bBlock curBlock;
-(void)loadRequest:(bBlock)comBlock;

@end
