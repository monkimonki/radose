//
//  UIFont+RD.h
//  Radose
//
//  Created by mm7 on 18.07.14.
//  Copyright (c) 2014 mm7. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIFont (RD)
+(UIFont*)RDFontMedium;
+(UIFont*)RDFontBig;
+(UIFont*)RDFontBigBold;
+(UIFont*)RDFontSmall;
@end
