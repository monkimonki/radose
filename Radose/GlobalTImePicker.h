//
//  GlobalTImePicker.h
//  TutoringOnDemand
//
//  Created by Aleksandr Padalko on 3/17/14.
//  Copyright (c) 2014 mm7. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol GlobalTImePickerDelegate <NSObject>

-(void) picked: (NSString *) val;
-(void) pickedDate: (NSDate*) val;

@end
@interface GlobalTImePicker : UIView<UIPickerViewDelegate,UIPickerViewDataSource>{
    UIDatePicker * pv;
	NSDate * limitDateTime;
}
@property (assign, nonatomic)id<GlobalTImePickerDelegate> pickerDelegate;
+(GlobalTImePicker*) createGlobalTimePicker;


@property (retain,nonatomic) NSString * minDate;
@property (retain,nonatomic) NSString * maxDate;
@end
