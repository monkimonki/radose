//
//  UIFont+RD.m
//  Radose
//
//  Created by mm7 on 18.07.14.
//  Copyright (c) 2014 mm7. All rights reserved.
//

#import "UIFont+RD.h"

@implementation UIFont (RD)
+(UIFont*)RDFontMedium{
    return [UIFont systemFontOfSize:12];
}
+(UIFont*)RDFontBig{
    return [UIFont systemFontOfSize:18];
}
+(UIFont*)RDFontBigBold{
    return [UIFont boldSystemFontOfSize:18];
}
+(UIFont*)RDFontSmall{
    return [UIFont systemFontOfSize:10];
}
@end
