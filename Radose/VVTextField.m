//
//  KramTextField.m
//  TutoringOnDemand
//
//  Created by mm7 on 13.03.14.
//  Copyright (c) 2014 mm7. All rights reserved.
//

#import "VVTextField.h"
@interface VVTextField(){
    CALayer * selectionBorder;
    
    UIActivityIndicatorView * loadView;
}
@end
@implementation VVTextField

-(id)initWithCoder:(NSCoder *)aDecoder{
    
    if (self=[super initWithCoder:aDecoder]) {
    
        
        [self viewSetup];
          
      
    }
    return self;
}
-(id)initWithFrame:(CGRect)frame{
    
    if (self=[super initWithFrame:frame]) {

        
        
        [self viewSetup];
    
        
    }
    return self;
}

-(void)viewSetup{
    
    self.hidden=NO;
    
    self.placeholderString=self.placeholder;
    
    self.baseColor=[UIColor blackColor];
    [self setFont:[UIFont systemFontOfSize:[UIFont systemFontSize]]];
    
    [self setBackgroundColor:[UIColor clearColor]];
    [self setBorderStyle:UITextBorderStyleNone];
    _errorColor=[UIColor redColor];
    
    _borderColor=[UIColor blackColor];
    self.layer.borderColor=[UIColor clearColor].CGColor;
}
-(void)setSelectionBorderColor:(UIColor *)selectionBorderColor{
    
    _selectionBorderColor=selectionBorderColor;

}
-(void)setOk{
    if (xImageView) {
        [xImageView removeFromSuperview];
         xImageView=nil;
    }
    if (!okImageView) {
       
        okImageView=[[UIImageView alloc]initWithImage:_okImg];
        [okImageView setFrame:CGRectMake(self.frame.size.width-_okImg.size.width/2-15, self.frame.size.height/2-_okImg.size.height/4, _okImg.size.width/2, _okImg.size.height/2)];
        
        [self addSubview:okImageView];
        
        
            [self setNeedsDisplay];
    }
}

-(void)setX{
    if (okImageView) {
        [okImageView removeFromSuperview];
         okImageView=nil;
    }
    if (!xImageView) {
        
        xImageView=[[UIImageView alloc]initWithImage:_xImg];
        [xImageView setFrame:CGRectMake(self.frame.size.width-_xImg.size.width/2-15, self.frame.size.height/2-_xImg.size.height/4, _xImg.size.width/2, _xImg.size.height/2)];
        
        [self addSubview:xImageView];
        
        
        [self setNeedsDisplay];
    }
}

-(void)setErrorString:(NSString *)errorString{
    if (!_errorLabel) {
  

        [self createErrorLabel:errorString];

   
    [self addSubview:_errorLabel];
    }
    if (okImageView) {
        [okImageView removeFromSuperview];
        okImageView=nil;
    }
    _errorString=errorString;
    [_errorLabel setText:errorString];
    [self setNeedsDisplay];
    
    
}

-(UILabel*)createErrorLabel:(NSString*)errorString{
    UIFont * f;
    if (_errorFont) {
        f=_errorFont;
    }else{
        f=[UIFont systemFontOfSize:[UIFont systemFontSize]];
    }
    
    
    CGSize s=[errorString sizeWithFont:f];
    _errorLabel=[[UILabel alloc]initWithFrame:CGRectMake(self.frame.size.width-15-s.width, 0, s.width, self.frame.size.height)];
    [_errorLabel setTextColor:_errorColor];
    [_errorLabel setTextAlignment:NSTextAlignmentRight];
    [_errorLabel setFont:f];
    [_errorLabel setBackgroundColor:[UIColor clearColor]];
    
    
    return _errorLabel;
    
}
-(CGRect)textRectForBounds:(CGRect)bounds{
    if (_errorLabel) {
        
   
    return [super textRectForBounds:CGRectMake(bounds.origin.x, bounds.origin.y, bounds.size.width-_errorLabel.frame.size.width-16, bounds.size.height)];
    }else if(okImageView){
      return [super textRectForBounds:CGRectMake(bounds.origin.x, bounds.origin.y, bounds.size.width-okImageView.frame.size.width-2, bounds.size.height)];
    }else if(xImageView){
        return [super textRectForBounds:CGRectMake(bounds.origin.x, bounds.origin.y, bounds.size.width-xImageView.frame.size.width-2, bounds.size.height)];
    }else{
           return   [super textRectForBounds:bounds];
    }
}
-(CGRect)editingRectForBounds:(CGRect)bounds{
    
    
      return   [super editingRectForBounds:bounds];
    
    if (_errorLabel) {
        
        
        return [super editingRectForBounds:CGRectMake(bounds.origin.x, bounds.origin.y, bounds.size.width-_errorLabel.frame.size.width-2, bounds.size.height)];
    }else{
        
        return   [super editingRectForBounds:bounds];
    }
}
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetStrokeColorWithColor(context, [_borderColor CGColor]);
    
    if (_topBorder) {
    CGContextSetLineWidth(context, 1.0);
    CGContextMoveToPoint(context, 0, 0);
    CGContextAddLineToPoint(context, rect.size.width, 0);
    }
    
    if (_botBorder) {
  
    CGContextSetLineWidth(context, 1.0);
    CGContextMoveToPoint(context, 0, rect.size.height);
    CGContextAddLineToPoint(context, rect.size.width,rect.size.height);
        
    }
    if (_rightBorder) {
        
        CGContextSetLineWidth(context, 1.0);
        CGContextMoveToPoint(context, rect.size.width, 0);
        CGContextAddLineToPoint(context, rect.size.width,rect.size.height);
        
    }
    CGContextDrawPath(context, kCGPathStroke);
}
-(void)drawSelectionBorder{
    
}
#pragma mark - TextField Helper Delegate

-(void)textFieldDidBeginEditing:(UITextField *)textField{
    if (_errorLabel) {
        [_errorLabel removeFromSuperview];
        _errorLabel=nil;
    }
    if (xImageView) {
        [xImageView removeFromSuperview];
        xImageView=nil;
    }
    if (okImageView) {
        [okImageView removeFromSuperview];
        okImageView=nil;
    }
    if(_selectionBorderColor){
        self.layer.borderColor=_selectionBorderColor.CGColor;

    }
    
    [super textFieldDidBeginEditing:textField];
}

-(void)textFieldDidEndEditing:(UITextField *)textField{
    
    if(_selectionBorderColor){
        self.layer.borderColor=[UIColor clearColor].CGColor;

    }
    
    [super textFieldDidEndEditing:textField];
    
}

#pragma mark- load Additions
-(void)setIsLoad:(BOOL)isLoad{
    _isLoad=isLoad;
    if (_isLoad) {
        [self startLoad];
    }else{
        [self stopLoad];
    }
    
    
}
-(void)startLoad{
    [self setUserInteractionEnabled:NO];
    if(!loadView){
    loadView=[[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        
        float x=20;
        if (self.leftOffset){
            x+=self.leftOffset;
        }
        if (staticlabel) {
            x+=staticlabel.frame.size.width;
        }
    [loadView setCenter:CGPointMake(x, self.frame.size.height/2)];
    [loadView startAnimating];
        
        [self setPlaceholder:nil];
    [self addSubview:loadView];
    }
    
}

-(void)stopLoad{
            [self setPlaceholder:self.placeholderString];
    [loadView stopAnimating];
    [loadView removeFromSuperview];
    loadView=nil;
    [self setUserInteractionEnabled:YES];
}


@end
