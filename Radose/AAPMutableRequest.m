//
//  AAPMutableRequest.m
//  Chooose
//
//  Created by mm7 on 01.02.14.
//  Copyright (c) 2014 mm7. All rights reserved.
//

#import "AAPMutableRequest.h"

@implementation AAPMutableRequest

NSString * getMethodNamaeByType(int t){
    NSString* mt;
    switch (t) {
        case kMethodTypeGet:
            mt=@"GET";
            break;
            
        case KMethodTypePost:
            mt=@"POST";
            break;
        case kMethodTypePut:
            mt=@"PUT";
            break;
        case kMethodTypeDelete:
            mt=@"DELETE";
            break;
    }
    
    
    return mt;
    
}
-(void)setMethod:(int)method{
    
    _method=method;
    
    [self setHTTPMethod:getMethodNamaeByType(_method)];
}


-(void)setBodyAsDictionary:(NSDictionary *)dic{

        
        [self setHTTPBody:[AAPMutableRequest encodeDictionary:dic]];
        

}
+(NSData*)encodeDictionary:(NSDictionary*)dictionary {
    NSMutableArray *parts = [[NSMutableArray alloc] init];
    for (NSString *key in dictionary) {

            

        NSString *encodedValue = [[dictionary objectForKey:key] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSString *encodedKey = [key stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSString *part = [NSString stringWithFormat: @"%@=%@", encodedKey, encodedValue];
        [parts addObject:part];
            
        
    }
    NSString *encodedDictionary = [parts componentsJoinedByString:@"&"];
    NSLog(@"%@",encodedDictionary);
    
    
    return [encodedDictionary dataUsingEncoding:NSUTF8StringEncoding];
}

-(id)initWithString:(NSString*)str{
    if (self=[super initWithURL:[NSURL URLWithString:str]]) {
        
    }
    
    return self;
}


@end
