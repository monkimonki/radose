//
//  RDErorr.h
//  Radose
//
//  Created by mm7 on 18.07.14.
//  Copyright (c) 2014 mm7. All rights reserved.
//

#import <Foundation/Foundation.h>
enum kErorrTypes{
    
    kErorrTypeDuplicateEmail,
    kErorrTypeConnectionErorr,
    kErorrTypeWrongEmailOrPassword
};
@interface RDErorr : NSObject

+(RDErorr*)createWithType:(enum kErorrTypes)t;
@property (nonatomic)enum kErorrTypes erorrType;
@property(nonatomic,retain)NSString* erorrText;
@end
