//
//  AAPLabel.m
//  Chooose
//
//  Created by mm7 on 30.01.14.
//  Copyright (c) 2014 mm7. All rights reserved.
//

#import "AAPLabel.h"
@implementation NSString (UIColorTrasform)

-(UIColor*)colorTransform{
    NSArray *parts = [self componentsSeparatedByString:@" "];
    return [UIColor colorWithRed:[parts[0] floatValue] green:[parts[1] floatValue] blue:[parts[2] floatValue] alpha:[parts[3] floatValue]];
}

@end
@implementation UIColor (NSStringTrasform)

-(NSString*)stringTransform{
    return  [[CIColor colorWithCGColor:[self CGColor]] stringRepresentation];
}

@end
@interface AAPLabel(){
    
    NSMutableAttributedString * atributedString;
    NSMutableString * text;
    NSMutableArray * attributesArr;
    
    float baseWidth;
}
@end
@implementation AAPLabel

- (id)initWithPos:(CGPoint)pos andWidth:(float)w
{
    self = [super initWithFrame:CGRectMake(pos.x, pos.y,w, 0)];
    if (self) {
        baseWidth=w;
        text=[[NSMutableString alloc]init];
        attributesArr =[[NSMutableArray alloc]init];
        [self setBackgroundColor:[UIColor clearColor]];
        [self setNumberOfLines:20];
    }
    return self;
}
-(void)addString:(NSString *)str withColor:(UIColor *)color  andFont :(UIFont *)f{

    [attributesArr addObject:[NSDictionary dictionaryWithObjectsAndKeys:[NSDictionary dictionaryWithObjectsAndKeys:f.fontName,@"name",[NSString stringWithFormat:@"%f",f.pointSize],@"size", nil],@"font",[color stringTransform],@"color",[@(str.length) stringValue],@"size",@"text",@"type",nil]];
    
    [text appendString:str];
    
}

-(void)addString:(NSString *)str withColor:(UIColor *)color  andFont :(UIFont *)f withButton:(UIButton*)but{
    
    [attributesArr addObject:[NSDictionary dictionaryWithObjectsAndKeys:[NSDictionary dictionaryWithObjectsAndKeys:f.fontName,@"name",[NSString stringWithFormat:@"%f",f.pointSize],@"size", nil],@"font",[color stringTransform],@"color",[@(str.length) stringValue],@"size",@"text",@"type",but,@"button", nil]];
    
    [text appendString:str];
    
}
-(void)clean{
    text= nil;
       text=[[NSMutableString alloc]init];
    [self setFrame:CGRectMake(self.frame.origin.x, self.frame.origin.y, baseWidth, 1)];
    [attributesArr removeAllObjects];
    
    [self.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    
}
-(void)addLineSpace{
    [attributesArr addObject:[NSDictionary dictionaryWithObjectsAndKeys:@"lineSpace",@"type", nil]];
    
    
}
-(void)setLabel{
    
      atributedString=[[NSMutableAttributedString alloc]initWithString:text];
    int deltaX=0;
    for (int a=0; a<attributesArr.count; a++) {
        
        NSDictionary * attribute=[attributesArr objectAtIndex:a];
        if ([[attribute valueForKey:@"type"]isEqualToString:@"text"]) {
            
       
        NSDictionary * fontAtribute=[attribute valueForKey:@"font"];
        
        NSString * fontName=[fontAtribute valueForKey:@"name"];
        float fontSize=[[fontAtribute valueForKey:@"size"] floatValue];
        //
        
        UIColor * color=[[attribute valueForKey:@"color"] colorTransform];
        float size=[[attribute valueForKey:@"size"] integerValue];
        
        
        
        [atributedString addAttribute:NSForegroundColorAttributeName value:color range:NSMakeRange(deltaX,size)];
        [atributedString addAttribute:NSFontAttributeName value:[UIFont fontWithName:fontName size:fontSize] range:NSMakeRange(deltaX,size)];
            
            if ([attribute valueForKey:@"button"]) {
                UIButton * but=[attribute valueForKey:@"button"];
                [but setBackgroundColor:[UIColor orangeColor]];
                [but setAlpha:0.5];
                
                [but setFrame:CGRectMake(deltaX, 0, size*fontSize, fontSize)];
                
                [self addSubview:but];
                
            }
        
        deltaX+=size;
        }else{
            
            
            NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle  alloc] init];
            paragraphStyle.maximumLineHeight = 20;
            [atributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(deltaX, 1)];
        }
    }
   
    [self setAttributedText:atributedString];
    [self sizeToFit];
    
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
