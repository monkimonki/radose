//
//  RDTableCell.h
//  Radose
//
//  Created by mm7 on 30.07.14.
//  Copyright (c) 2014 mm7. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RDTableCell : UITableViewCell
+(CGSize)cellSize;
+(void)setCellSize:(CGSize)size;
@end
