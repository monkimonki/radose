//
//  RequestToServer.m
//  Chooose
//
//  Created by mm7 on 01.02.14.
//  Copyright (c) 2014 mm7. All rights reserved.
//

#import "RequestToServer.h"

@interface RequestToServer () {

}

@end

@implementation RequestToServer


-(id)initWithReq:(AAPMutableRequest*)req{
    if (self=[super init]) {
        
        request=req;
        requestData=[[NSMutableData alloc]init];
      
    }
 
    return self;
}
-(void)setRequestBodyData:(id)data{
    [request setHTTPBody:data];
    
}

-(void)loadRequest{
       NSURLConnection * con=[[NSURLConnection alloc]initWithRequest:request delegate:self];
    NSLog(@"%@",request.URL);
    [con start];
}
-(void)loadRequest:(bBlock)comBlock
{
    _curBlock=comBlock;
    [self loadRequest];
}



#pragma  mark - NSURLConnection Delegate

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response{
    NSLog(@"didReceiveResponse");
    NSHTTPURLResponse * httpResponse = (NSHTTPURLResponse *) response;
    errorCode = httpResponse.statusCode;
    NSString *fileMIMEType = [[httpResponse MIMEType] lowercaseString];
    NSLog(@"didReceiveResponse\nErorrCode:%d\ndiscr:%@",errorCode,fileMIMEType);
    
}
- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data{
    NSLog(@"didReceiveData");
    [requestData appendData:data];
    
    
}
- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error{
    NSLog(@"didFailWithError");
    NSLog(@"%@",error);
    if (_curBlock) {
         _curBlock(NO,nil);
        _curBlock=nil;
    }
  
    [self.requestToServerDelegate failed];
}
-(void)connectionDidFinishLoading:(NSURLConnection *)connection{
    NSArray * kk=[NSJSONSerialization JSONObjectWithData:requestData options:NSJSONReadingMutableContainers error:nil];
    if (!kk) {
            NSString * str=[[NSString alloc] initWithData:requestData encoding:NSUTF8StringEncoding];
        if (str) {
         kk=   [NSDictionary dictionaryWithObject:str forKey:@"response"];
        }
        
    }

    
    
    [self.requestToServerDelegate sucess:kk];
    
    if (_curBlock&&kk) {
        

        _curBlock(YES,kk);
        _curBlock=nil;
    }else{
        if (_curBlock) {
            _curBlock(NO,nil);
            _curBlock=nil;
        }
    }
    
}
@end
