//
//  RDTabBar.h
//  Radose
//
//  Created by mm7 on 18.07.14.
//  Copyright (c) 2014 mm7. All rights reserved.
//

#import <UIKit/UIKit.h>
@class RDTabBar;
@protocol RDTabBarDelegate <NSObject>
-(void)rdTabBar:(RDTabBar*)tabbar didSelectButtonAtIndex:(int)idx;
@end
@interface RDTabBar : UIView
+(RDTabBar*)createBar;

@property (nonatomic)int selectedIndex;
@property (assign,nonatomic)id<RDTabBarDelegate >delegate;
@end
