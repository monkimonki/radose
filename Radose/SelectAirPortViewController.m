//
//  SelectAirPortViewController.m
//  Radose
//
//  Created by mm7 on 19.07.14.
//  Copyright (c) 2014 mm7. All rights reserved.
//

#import "SelectAirPortViewController.h"
#import "RDTextField.h"
#import "FGLineView.h"
#import "ArportUpdater.h"
#import "AAPLabel.h"
@interface SelectAirPortViewController ()<UITableViewDataSource,UITableViewDelegate,VVTextFieldProtocol>{
    RDTextField * searchField;
    
    UITableView * airTableView;
    BOOL searching;
    NSMutableArray * searchingArr;
}

@end

@implementation SelectAirPortViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [[RDNavigationController sharedInstance]hideLogoWithAnitmation:YES
                                                         andCompl:^{
                                                             
                                                         }];

}
-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [[RDNavigationController sharedInstance]showLogoWithAnimation:YES
                                                          andCompl:^{
                                                              
                                                          }];
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    searching=NO;
    [self createLeftBackButton];
    if (![RDSingleton sharedInstance].airportsArray) {
        [ArportUpdater updateAirPorts:^(BOOL suc, RDErorr *err) {
            if (suc) {
                [self createUI];
            }else{
                [self.navigationController popViewControllerAnimated:YES];
            }
        }];
    }else{
        [self createUI];
    }
    
    
  
    
    // Do any additional setup after loading the view.
}
-(void)createUI{
    searchField=[RDTextField createBase];
    [searchField setPlaceholder:[locSearchPlaceHolder localized]];
    [searchField addImage:[UIImage imageNamed:@""]];
    [searchField setFrame:CGRectMake(43, 58, self.view.frame.size.width-43*2, 40)];
    [searchField setBackgroundColor:[UIColor clearColor]];
    searchField.layer.borderWidth=0.0;
    searchField.layer.cornerRadius=0.0;
    searchField.layer.borderColor=[UIColor clearColor].CGColor;
    [searchField setVvTextFieldDelegate:self];
    
    [self.view addSubview:searchField];
    FGLineView * lv=[[FGLineView alloc]initWithFrame:CGRectMake(0, searchField.frame.origin.y, self.view.frame.size.width, 1)];
    [lv setLineColor:[UIColor whiteColor]];
    [self.view addSubview:lv];
    FGLineView * lv2=[[FGLineView alloc]initWithFrame:CGRectMake(0, searchField.frame.origin.y+searchField.frame.size.height-1, self.view.frame.size.width, 1)];
    [lv2 setLineColor:[UIColor whiteColor]];
    [self.view addSubview:lv2];
    
    airTableView=[[UITableView alloc]init];
    [airTableView setDataSource:self];
    [airTableView setDelegate:self];
    [airTableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    [airTableView setBackgroundColor:[UIColor clearColor]];
    [self.view addSubview:airTableView];
    
}
-(void)viewDidLayoutSubviews{
    [super viewDidLayoutSubviews];
    [airTableView setFrame:CGRectMake(0,searchField.frame.origin.y+searchField.frame.size.height, self.view.frame.size.width, self.view.frame.size.height-[searchField getLastY])];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
#pragma mark - table view

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    if (searching) {
        return MIN(searchingArr.count, 500);
    }
    
    return MIN([RDSingleton sharedInstance].airportsArray.count, 500);
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
  static  NSString * indif=@"as";
    UITableViewCell * cell=[tableView dequeueReusableCellWithIdentifier:indif];
    
    if (!cell) {
        cell=[[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:indif];
        [cell setBackgroundColor:[UIColor clearColor]];
        FGLineView * lv=[[FGLineView alloc]initWithFrame:CGRectMake(0, 46-1, self.view.frame.size.width, 1)];
        [cell addSubview:lv];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        AAPLabel  * lab=[[AAPLabel alloc]initWithPos:CGPointMake(15,5) andWidth:self.view.frame.size.width-30];
        [lab setTag:1];
        [cell addSubview:lab];

    }
    NSArray * curArr;
    if (searching) {
        curArr=searchingArr;
    }else{
        curArr=[RDSingleton sharedInstance].airportsArray ;
    }
    NSDictionary * d=[curArr objectAtIndex:indexPath.row];
    
    AAPLabel * l=[cell viewWithTag:1];
    [l clean];
    

    
    [l addString:[NSString stringWithFormat:@"%@, ",[d valueForKey:@"iata_code"]] withColor:[UIColor whiteColor] andFont:[UIFont RDFontBigBold]];
  //  NSLog(@"%@",[[NSLocale preferredLanguages] objectAtIndex:0]);
    
    if ( [[[NSLocale preferredLanguages] objectAtIndex:0] isEqualToString:@"ru"]) {
        if ([d valueForKey:@"city_rus"]) {
            [l addString:[d valueForKey:@"name_rus"] withColor:[UIColor whiteColor] andFont:[UIFont RDFontMedium]];
        }else
        [l addString:[d valueForKey:@"name_eng"] withColor:[UIColor whiteColor] andFont:[UIFont RDFontMedium]];
        
        
        [l addString:@", "  withColor:[UIColor whiteColor] andFont:[UIFont RDFontMedium]];
        
        
           [l addString:[d valueForKey:@"country_rus"]  withColor:[UIColor whiteColor] andFont:[UIFont RDFontMedium]];
              [l addString:@", "  withColor:[UIColor whiteColor] andFont:[UIFont RDFontMedium]];
        
                [l addString:[d valueForKey:@"city_rus"]  withColor:[UIColor whiteColor] andFont:[UIFont RDFontMedium]];
        
    }else{
        [l addString:[d valueForKey:@"name_eng"] withColor:[UIColor whiteColor] andFont:[UIFont RDFontMedium]];
        
        
        [l addString:@", "  withColor:[UIColor whiteColor] andFont:[UIFont RDFontMedium]];
        
        
        [l addString:[d valueForKey:@"country_eng"]  withColor:[UIColor whiteColor] andFont:[UIFont RDFontMedium]];
        [l addString:@", "  withColor:[UIColor whiteColor] andFont:[UIFont RDFontMedium]];
        
        [l addString:[d valueForKey:@"city_eng"]  withColor:[UIColor whiteColor] andFont:[UIFont RDFontMedium]];

    }
    
    [l setLabel];
    
    return cell;
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 54;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSDictionary * d;
    if (searchingArr) {
        d=[searchingArr objectAtIndex:indexPath.row];
    }else{
        d=[[RDSingleton sharedInstance].airportsArray objectAtIndex:indexPath.row];
    }
    
    
    [self.delegate didSelectAirpord:d];
}
#pragma mark - text field delegate 

-(void) editingBegin:(UIView*)v{
    
}
-(void) editingEndWithView:(UIView*)v{
    
}
-(void) editingEnd{
    
}
-(void) editingEndWithText:(NSString*)str{

}

-(void)clear{
    
}
-(void)clearWithObj:(id)obj{
    
}


-(void)editingWithText:(NSString*)text{
    
    if (text.length>0) {
        searching=YES;
        [self search:text];
    }else{
        searchingArr=nil;
        searching=NO;
          [airTableView reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationFade];
    }
    
}


-(void)textFieldShouldTabBack:(UITextField*)tf{
    
}
-(void)textFieldShouldTabForward:(UITextField*)tf{
    
}
#pragma mark - search func
-(void)search:(NSString*)text{
    
//    searchingArr = [NSMutableArray arrayWithCapacity:[RDSingleton sharedInstance].airportsArray.count];
//    
//    for (NSDictionary* place in [RDSingleton sharedInstance].airportsArray)
//    {
//        NSRange nameRange = [[place valueForKey:@"name_eng"] rangeOfString:text options:NSCaseInsensitiveSearch];
//        
//        if(nameRange.location != NSNotFound)
//        {
//            [searchingArr addObject:place];
//        }
//    }
//    
//    return;
    //NSPredicate *p = [NSPredicate predicateWithFormat:@"name_eng contains[cd] %@", text];
        NSPredicate *p = [NSPredicate predicateWithFormat:@"name_eng contains[cd] %@ OR name_rus contains[cd]%@ OR city_eng contains[cd]%@ OR city_rus contains[cd]%@ OR iata_code contains[cd]%@", text,text, text,text, text];
  //  NSLog(@"start");
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT
                                           , 0), ^{
        //   NSLog(@"start search");
        __block   NSArray *arr;
        if (!searchingArr) {
            arr = [[RDSingleton sharedInstance].airportsArray filteredArrayUsingPredicate:p];
        }else{
            arr = [searchingArr filteredArrayUsingPredicate:p];
        }
        
  //         NSLog(@"end search");
        dispatch_async(dispatch_get_main_queue(), ^{
         //    NSLog(@"in");
            searchingArr=[[NSMutableArray alloc]initWithArray:arr];
           //  NSLog(@"reload");
             [airTableView reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationFade];
        });
        
        
    });
   //   NSLog(@"end");
  
 
   
//    NSMutableArray * arr=[[NSMutableArray alloc]init];
//    for (UITableViewCell*c in [airTableView visibleCells]) {
//        [arr addObject:[airTableView indexPathForCell:c]];
//    }
//    [airTableView visibleCells];
//    [airTableView reloadData];
//    [airTableView reloadRowsAtIndexPaths:arr withRowAnimation:UITableViewRowAnimationFade];
//    [airTableView removeFromSuperview];
//    airTableView=Nil;
//    airTableView=[[UITableView alloc]init];
//    [airTableView setDataSource:self];
//    [airTableView setDelegate:self];
//    [airTableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
//    [airTableView setBackgroundColor:[UIColor clearColor]];
//    [self.view addSubview:airTableView];
//        [airTableView setFrame:CGRectMake(0,searchField.frame.origin.y+searchField.frame.size.height, self.view.frame.size.width, self.view.frame.size.height-[searchField getLastY])];
    
}
@end
