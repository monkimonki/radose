//
//  ArportUpdater.h
//  Radose
//
//  Created by mm7 on 19.07.14.
//  Copyright (c) 2014 mm7. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ArportUpdater : UIView
+(void)updateAirPorts:(void(^)(BOOL suc,RDErorr*err))block;
@end
