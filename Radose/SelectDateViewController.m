//
//  SelectDateViewController.m
//  Radose
//
//  Created by mm7 on 19.07.14.
//  Copyright (c) 2014 mm7. All rights reserved.
//

#import "SelectDateViewController.h"
#import "CalendarView.h"

#import "RDButton.h"
@interface SelectDateViewController ()<CalendarDelegate>
{
    CalendarView * calendar;
    NSDate * sDate;
    RDButton  *r ;
}
@end

@implementation SelectDateViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    CGRect scr=[UIScreen mainScreen].bounds;
    if (scr.size.height==480) {
    [[RDNavigationController sharedInstance]hideLogoWithAnitmation:YES
                                                          andCompl:^{
                                                              
                                                          }];
    }
    
}
-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    CGRect scr=[UIScreen mainScreen].bounds;
    if (scr.size.height==480) {
    [[RDNavigationController sharedInstance]showLogoWithAnimation:YES
                                                         andCompl:^{
                                                             
                                                         }];
    }
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    CGRect scr=[UIScreen mainScreen].bounds;
    if (scr.size.height==480) {
          calendar= [[CalendarView alloc]initWithFrame:CGRectMake(0, 60, self.view.bounds.size.width, 250)];
    }else
    calendar= [[CalendarView alloc]initWithFrame:CGRectMake(0, self.view.frame.size.height/4+20, self.view.bounds.size.width, 250)];
    calendar.delegate = self;
    [calendar setBackgroundColor:[UIColor clearColor]];
    calendar.calendarDate = [NSDate date];
      [self.view addSubview:calendar];
    
    [self createLeftBackButton];
     r =[RDButton createWithTitle:[locDone localized]];
    [r setGreen];
    [r setCenter:CGPointMake(self.view.frame.size.width/2, calendar.frame.size.height+calendar.frame.origin.y+r.frame.size.height/2+10)];
    [r setAlpha:0];
    [self.view addSubview:r];
    
    [r addTarget:self action:@selector(done) forControlEvents:UIControlEventTouchUpInside];
    
    // Do any additional setup after loading the view.
}
-(void)done{
    [self.delegate didSelectDate:sDate];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)tappedOnDate:(NSDate *)selectedDate
{
    
    if (r.alpha==0) {
        [UIView animateWithDuration:0.33 animations:^{
            [r setAlpha:1];
        }];
    }
    sDate=selectedDate;    NSLog(@"tappedOnDate %@(GMT)",selectedDate);
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
