//
//  RDTabBarViewController.m
//  Radose
//
//  Created by mm7 on 17.07.14.
//  Copyright (c) 2014 mm7. All rights reserved.
//

#import "RDTabBarViewController.h"
#import "RDTabBar.h"

#import "StatViewController.h"
#import "AboutViewController.h"
#import "DPFViewController.h"
#import "ArportUpdater.h"
#import "AccViewController.h"
@interface RDTabBarViewController ()<RDTabBarDelegate>{
    RDTabBar * tabBar;
}

@end

@implementation RDTabBarViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    if (![RDSingleton sharedInstance].airportsArray) {
        [ArportUpdater updateAirPorts:^(BOOL suc, RDErorr *err) {
            
        }];
    }
}
- (void)viewDidLoad
{
    [super viewDidLoad];

    [self.view setBackgroundColor:[UIColor clearColor]];
    [self.tabBar setHidden:YES];
    
  tabBar= [RDTabBar createBar];
    
    [tabBar setFrame:CGRectMake(0, self.view.frame.size.height-tabBar.frame.size.height,tabBar.frame.size.width, tabBar.frame.size.height)];
    [self.view addSubview:tabBar];
    [tabBar setDelegate:self];
    [self setViewControllers:[NSArray arrayWithObjects:[[AboutViewController alloc] init],[[RDNavigationController alloc]initSubControllerWithRoot:[[DPFViewController alloc] init]],[[StatViewController alloc] init],[[AccViewController alloc] init], nil]];
    [tabBar setSelectedIndex:1];
    
    
     
    
    // Do any additional setup after loading the view.
}

-(void)setViewControllers:(NSArray *)viewControllers{
    for (UIViewController * vc in viewControllers) {
        [vc.view setFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height-tabBar.frame.size.height)];
    }
    [super setViewControllers:viewControllers];
}
-(void)viewWillLayoutSubviews{
    [super viewWillLayoutSubviews];
    for (UIViewController * vc in self.viewControllers) {
        [vc.view setFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height-tabBar.frame.size.height)];
    }
}
-(void)viewDidLayoutSubviews{
    
    [super viewDidLayoutSubviews];
    for (UIViewController * vc in self.viewControllers) {
        [vc.view setFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height-tabBar.frame.size.height)];
    }
    
    
}
 
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - rdtabbar delegate
-(void)rdTabBar:(RDTabBar *)tabbar didSelectButtonAtIndex:(int)idx{
    
    [self setSelectedIndex:idx];
}
-(void)tabbarTimeAnimation{
    
    
    RDViewController* v=(RDViewController*)[self.viewControllers objectAtIndex:tabBar.selectedIndex];

    [v timeAnimation];
    
    
    
    [UIView animateWithDuration:0.05 animations:^{
          [tabBar setBackgroundColor:[UIColor colorWithRed:1 green:1 blue:1 alpha:0.7]];
    }completion:^(BOOL finished) {
        [UIView animateWithDuration:0.05 animations:^{
            [tabBar setBackgroundColor:[UIColor colorWithRed:0 green:0 blue:0 alpha:0.7]];
        }completion:^(BOOL finished) {
            [UIView animateWithDuration:0.1 animations:^{
                [tabBar setBackgroundColor:[UIColor colorWithRed:1 green:1 blue:1 alpha:0.7]];
            }completion:^(BOOL finished) {
                [UIView animateWithDuration:0.05 animations:^{
                    [tabBar setBackgroundColor:[UIColor colorWithRed:0 green:0 blue:0 alpha:0.7]];
                }completion:^(BOOL finished) {
                    
                }];
                
            }];
            
        }];
        
    }];
    
    
}

-(void)selectIndex:(int)idx{
    tabBar.selectedIndex=idx;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
