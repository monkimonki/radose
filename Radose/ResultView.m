//
//  ResultView.m
//  Radose
//
//  Created by mm7 on 20.07.14.
//  Copyright (c) 2014 mm7. All rights reserved.
//

#import "ResultView.h"
#import "FGLineView.h"
@interface ResultView(){
 
    UILabel * lab1;
    UILabel * lab2;
}
@end
@implementation ResultView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        FGLineView * lv=[[FGLineView alloc]initWithFrame:CGRectMake(0, 0, self.frame.size.width, 1)];
        [lv setLineColor:[UIColor RDGrayColor]];
        
        [self addSubview:lv];
        
        lab1 =[[UILabel alloc]initWithFrame:CGRectMake(0, 0, self.frame.size.width-15, 38)];
        [lab1 setTextColor:[UIColor RDOrangeColor]];
        [lab1 setTextAlignment:NSTextAlignmentLeft];
        [lab1 setFont:[UIFont RDFontMedium]];
        
        [self addSubview:lab1];
        
        
        lab2 =[[UILabel alloc]initWithFrame:CGRectMake(0, [lab1 getLastY], self.frame.size.width, self.frame.size.height-[lab1 getLastY])];
        [lab2 setTextColor:[UIColor whiteColor]];
        [lab2 setTextAlignment:NSTextAlignmentLeft];
        [lab2 setFont:[UIFont RDFontMedium]];
        [lab2 setNumberOfLines:2];
        [lab2 setText:[locSaveStatisticMessage localized]];
        
        [self addSubview:lab2];
        
        
        UIImageView * imgv=[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"Next"]];
        [imgv setFrame:CGRectMake(self.frame.size.width-imgv.frame.size.width, lab2.frame.origin.y, imgv.frame.size.width, imgv.frame.size.height)];
        [self addSubview:imgv];
        
        
        [self setAlpha:0];
        
        
    }
    return self;
}
-(void)setRadiance:(float)radiance{
    int r=(radiance/10)*100;
    
    NSString  * str=[NSString stringWithFormat:@"%.1f mSv = %d %% %@",radiance,r,[locOfDaylyRate localized]];
    
    [lab1 setText:str];
    
    
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
