//
//  RDTextField.h
//  Radose
//
//  Created by mm7 on 18.07.14.
//  Copyright (c) 2014 mm7. All rights reserved.
//

#import "VVTextField.h"

@interface RDTextField : VVTextField
+(RDTextField *)createBase;
-(void)addImage:(UIImage*)img;
@end
