//
//  LoadView.m
//  Radose
//
//  Created by mm7 on 21.07.14.
//  Copyright (c) 2014 mm7. All rights reserved.
//

#import "LoadView.h"
#import "AppDelegate.h"
@implementation LoadView

- (id)initWithFrame:(CGRect)frame andName:(NSString*)name
{
    self = [super initWithFrame:frame];
    if (self) {
        
        [self setBackgroundColor:[UIColor colorWithRed:0 green:0 blue:0 alpha:0.87]];
        
        UIActivityIndicatorView * v=[[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
        [v setCenter:CGPointMake(self.frame.size.width/2, self.frame.size.height/2)];
        [v startAnimating];
        [self addSubview:v];
        UILabel * lab=[[UILabel alloc]initWithFrame:CGRectMake(15, 15, self.frame.size.width-30, 200)];
        
        [lab setFont:[UIFont RDFontMedium]];
        [lab setTextAlignment:NSTextAlignmentCenter];
        [lab setTextColor:[UIColor whiteColor]];
        [self addSubview:lab];
        [lab setText:name];
        // Initialization code
    }
    return self;
}
-(void)appear{
    
    
    [self setAlpha:0];
    
    
    [UIView animateWithDuration:0.33
     
                     animations:^{
                         [self setAlpha:1];
                     }];
}


-(void)dissapper{
    
    
    
    
    [UIView animateWithDuration:0.33
     
                     animations:^{
                         [self setAlpha:0];
                     }];
}
+(LoadView*)createWithName:(NSString*)name{
    AppDelegate * ap=[UIApplication sharedApplication].delegate;
    
    
    LoadView* lv=[[LoadView alloc]initWithFrame:ap.window.bounds andName:name];
    
    [ap.window addSubview:lv];
    
    return lv;
    
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
