//
//  UIColor+RD.m
//  Radose
//
//  Created by mm7 on 18.07.14.
//  Copyright (c) 2014 mm7. All rights reserved.
//

#import "UIColor+RD.h"

@implementation UIColor (RD)
+(UIColor*)RDGreenColor{
    
    return [UIColor greenColor];
}
+(UIColor*)RDGrayColor{
    
    return [UIColor grayColor];
}
+(UIColor *)RDOrangeColor{
    return [UIColor orangeColor];
}
+(UIColor*)RDYellowColor{
    return [UIColor yellowColor];
}
@end
