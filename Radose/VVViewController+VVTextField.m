//
//  UIViewController+VVTextField.m
//  FGApp
//
//  Created by mm7 on 10.05.14.
//  Copyright (c) 2014 mm7. All rights reserved.
//

#import "VVViewController+VVTextField.h"
#import "AppDelegate.h"

@implementation RDViewController (VVTextField)
static float offset;
static CGSize  keyBoardSize;
static UIView  * curResponder;
static NSMutableArray * observArr;
static CGPoint baseCurResponderPos;
static UIView * curResponderSuperView;
static UIView *newCurResponder;
-(void)vvTextFieldAdded{
    
  //  id vc= [super init];
    
    if (!observArr) {
        observArr=[[NSMutableArray alloc]init];
    }
    if (![observArr containsObject:self]) {
        
 
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
        [observArr addObject:self];
   }
    
    
   // return vc;
    
    
}


-(void)textFieldShouldTabBack:(id)tf{
    NSUInteger idx=[self.textFieldsArray indexOfObject:tf];
    
    if (idx>0) {
        [[self.textFieldsArray objectAtIndex:idx-1] becomeFirstResponder];
    }
    
}
-(void)textFieldShouldTabForward:(id)tf{
    NSUInteger idx=[self.textFieldsArray indexOfObject:tf];
    
    if (idx<self.textFieldsArray.count-1) {
        [[self.textFieldsArray objectAtIndex:idx+1] becomeFirstResponder];
    }
}

- (void)keyboardWasHide:(NSNotification *)notification
{
     keyBoardSize=CGSizeZero;
    curResponder=nil;
//    if (offset>0) {
//          if ([self isKindOfClass:[VVListTableViewController class]]) {
//              
//          }else
//        [self moveObject:self.view andPos:CGPointMake(self.view.center.x, self.view.center.y+offset) andTime:0.33];
    [UIView animateWithDuration:0.33 animations:^{
               [self.view setFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)]; 
    }];

        
//    }
    offset=0;
}
-(void)keyboardWasShown:(NSNotification *)notification{
    keyBoardSize = [[[notification userInfo] objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    [self showAction];
}

-(void)showAction{
    
    curResponder=newCurResponder;
    AppDelegate * ap=(AppDelegate *)[UIApplication sharedApplication].delegate;
    
    
    float keyLastY=ap.window.frame.size.height-keyBoardSize.height;

    CGRect realPos=[ap.window convertRect:curResponder.frame fromView:curResponder.superview];
    float vLastY=realPos.origin.y+realPos.size.height;
    
    
    
    if (vLastY>=keyLastY) {
        offset=vLastY-keyLastY;
  
        offset+=10;
    [self moveObject:self.view andPos:CGPointMake(self.view.center.x, self.view.center.y-offset) andTime:0.33];
    }

//        if ([self isKindOfClass:[VVListTableViewController class]]) {
//            
//            
//            
//            VVListTableViewController * l=(VVListTableViewController*)self;
////            curResponderSuperView=curResponder.superview;
////             baseCurResponderPos=curResponder.frame.origin;
////            
////            UIView * bv=[[UIView alloc]initWithFrame:ap.window.bounds];
////            [bv setBackgroundColor:[UIColor colorWith8BitRed:0 green:0 blue:0 alpha:0.95]];
////            [bv setTag:1112];
////            [ap.window addSubview:bv];
////          
////           
//            [UIView animateWithDuration:0.33 animations:^{
//                  [l.listTableView setContentOffset:CGPointMake(l.listTableView.contentOffset.x, l.listTableView.contentOffset.y+offset) animated:NO];
//            }completion:^(BOOL finished) {
////                
////                [ap.window addSubview:bv];
////              CGRect nr=  [ap.window convertRect:curResponder.frame fromView:curResponder.superview];
////                [curResponder setFrame:nr];
////                [ap.window addSubview:curResponder];
//            }];
//        
//            
//        }else
//    }else{
//        offset=0;
//       [self moveObject:self.view andPos:CGPointMake(self.view.center.x, self.view.center.y-offset) andTime:0.33];
//    }
}
-(void)moveObject:(UIView *)v andPos:(CGPoint)pos andTime:(float)t {
    [UIView animateWithDuration:t animations:^{
        v.center = pos;
    }];
}
- (BOOL)textViewShouldBeginEditing:(UITextView *)textView{
    newCurResponder=textView;
    return YES;
}
- (BOOL)textViewShouldEndEditing:(UITextView *)textView{
    
    curResponder=nil;
    
    return YES;
}

-(void)editingBegin:(UIView *)v{
    newCurResponder=v;
    
}
-(void) editingEndWithView:(UIView*)v{
    
}
-(void) editingEnd{

//    if ([self isKindOfClass:[VVListTableViewController class]]) {
//        
//        
//        
////        
////          AppDelegate * ap=(AppDelegate *)[UIApplication sharedApplication].delegate;
////
////        UIView * bv=[ap.window viewWithTag:1112];
////    [UIView animateWithDuration:0.33 animations:^{
////        [bv setAlpha:0];
////       
////    }completion:^(BOOL finished) {
////        [curResponder setFrame:CGRectMake(baseCurResponderPos.x, baseCurResponderPos.y, curResponder.frame.size.width, curResponder.frame.size.height)];
////        [bv removeFromSuperview];
////        [curResponderSuperView addSubview:curResponder];
////
////    }];
//    }
    if ([curResponder isFirstResponder]) {
        [curResponder resignFirstResponder];
    }
        curResponder=nil;
    
}
-(void) editingEndWithText:(NSString*)str{
    
}

-(void)clear{
    
}
-(void)clearWithObj:(id)obj{
    
}


-(void)editingWithText:(NSString*)text{
    
}
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    if (curResponder) 
    [curResponder resignFirstResponder];
    [super touchesBegan:touches withEvent:event];
}

-(UIView *)curResponder
{
	return curResponder;
}

@end
