//
//  AAPLabel.h
//  Chooose
//
//  Created by mm7 on 30.01.14.
//  Copyright (c) 2014 mm7. All rights reserved.
//

#import <UIKit/UIKit.h>
@interface NSString (UIColorTrasform)

-(UIColor*)colorTransform;

@end

@interface UIColor (NSStringTrasform)

-(NSString*)stringTransform;

@end
@interface AAPLabel : UILabel
- (id)initWithPos:(CGPoint)pos andWidth:(float)w;

-(void)setLabel;
-(void)addString:(NSString*)str withColor:(UIColor*)color andFont:(UIFont*)f;
-(void)addLineSpace;
-(void)clean;
-(void)addString:(NSString *)str withColor:(UIColor *)color  andFont :(UIFont *)f withButton:(UIButton*)but;
@end
