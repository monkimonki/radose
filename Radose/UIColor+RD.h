//
//  UIColor+RD.h
//  Radose
//
//  Created by mm7 on 18.07.14.
//  Copyright (c) 2014 mm7. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (RD)
+(UIColor*)RDGreenColor;
+(UIColor*)RDOrangeColor;
+(UIColor*)RDYellowColor;
+(UIColor*)RDGrayColor;
@end
