
#import "CalendarView.h"

@interface CalendarView()

{
    
    NSCalendar *gregorian;
    NSInteger _selectedMonth;
    NSInteger _selectedYear;
}

@end
@implementation CalendarView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        UISwipeGestureRecognizer * swipeleft=[[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(swipeleft:)];
        swipeleft.direction=UISwipeGestureRecognizerDirectionLeft;
        [self addGestureRecognizer:swipeleft];
        UISwipeGestureRecognizer * swipeRight=[[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(swiperight:)];
        swipeRight.direction=UISwipeGestureRecognizerDirectionRight;
        [self addGestureRecognizer:swipeRight];
        UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(self.bounds.origin.x, self.bounds.size.height-100, self.bounds.size.width, 44)];
        [label setBackgroundColor:[UIColor brownColor]];
        [label setTextColor:[UIColor whiteColor]];
        [label setText:@"swipe to change months"];
        label.textAlignment = NSTextAlignmentCenter;
        [self addSubview:label];
        [UILabel beginAnimations:NULL context:nil];
        [UILabel setAnimationDuration:2.0];
        [label setAlpha:0];
        [UILabel commitAnimations];


    }
    return self;
}

- (void)drawRect:(CGRect)rect
{
    
    [self setCalendarParameters];
    _weekNames = @[@"MON",@"TUE",@"WED",@"THU",@"FRI",@"SAT",@"SUN"];
    NSDateComponents *components = [gregorian components:(NSEraCalendarUnit | NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit) fromDate:self.calendarDate];
//    _selectedDate  =components.day;
    components.day = 1;
    NSDate *firstDayOfMonth = [gregorian dateFromComponents:components];
    NSDateComponents *comps = [gregorian components:NSWeekdayCalendarUnit fromDate:firstDayOfMonth];
    int weekday = [comps weekday];
//      NSLog(@"components%d %d %d",_selectedDate,_selectedMonth,_selectedYear);
    weekday  = weekday - 2;
    
    if(weekday < 0)
        weekday += 7;
    
    NSCalendar *c = [NSCalendar currentCalendar];
    NSRange days = [c rangeOfUnit:NSDayCalendarUnit
                           inUnit:NSMonthCalendarUnit
                          forDate:self.calendarDate];
      NSInteger originX = 25;
    NSInteger columns = 7;
    NSInteger width = (rect.size.width-originX*2)/7;
    NSInteger spaceFromW=30;
    NSInteger originY = 40;
    NSUInteger height=width*0.8;
    NSInteger monthLength = days.length;
    
    UILabel *titleText;
    {
    titleText = [[UILabel alloc]initWithFrame:CGRectMake(originX,6, self.bounds.size.width/2, 30)];
    [titleText setBackgroundColor:[UIColor clearColor]];
    titleText.textAlignment = NSTextAlignmentCenter;
    NSDateFormatter *format = [[NSDateFormatter alloc] init];
    [format setDateFormat:@"MMMM"];
    NSString *dateString = [[format stringFromDate:self.calendarDate] uppercaseString];
    [titleText setText:dateString];
    [titleText setFont:[UIFont RDFontBig]];
    [titleText setTextColor:[UIColor whiteColor]];
    [titleText sizeToFit];
    UIImageView * imgv=[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"Forward"]];
    [titleText addSubview:imgv];
    [imgv setFrame:CGRectMake(titleText.frame.size.width+2, titleText.frame.size.height/2-imgv.frame.size.height/2, imgv.frame.size.width, imgv.frame.size.height)];
    
    
    UIImageView * imgv2=[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"Back"]];
    [titleText addSubview:imgv2];
    [imgv2 setFrame:CGRectMake(-2-imgv2.frame.size.width, titleText.frame.size.height/2-imgv2.frame.size.height/2, imgv2.frame.size.width, imgv2.frame.size.height)];
    
    [titleText setCenter:CGPointMake(self.frame.size.width/4+originX, titleText.center.y+5)];
    

    

    
    [self addSubview:titleText];
    UIButton * bLeft=[UIButton buttonWithType:UIButtonTypeCustom];
    
    [bLeft setFrame:CGRectMake(titleText.frame.origin.x-15, titleText.frame.origin.y-10, titleText.frame.size.width/2+15, titleText.frame.size.height+20)];
        [self addSubview:bLeft];
    //    [bLeft setBackgroundColor:[UIColor grayColor]];
    [bLeft addTarget:self action:@selector(monthLeft) forControlEvents:UIControlEventTouchUpInside];
    UIButton * bRight=[UIButton buttonWithType:UIButtonTypeCustom];
    
    [bRight setFrame:CGRectMake(titleText.frame.size.width/2+titleText.frame.origin.x, titleText.frame.origin.y-10, titleText.frame.size.width/2+15, titleText.frame.size.height+20)];
  //  [bRight setBackgroundColor:[UIColor purpleColor]];
    [self addSubview:bRight];
    [bRight addTarget:self action:@selector(monthRight) forControlEvents:UIControlEventTouchUpInside];
    
    }
    {
           UILabel * titleText2 = [[UILabel alloc]initWithFrame:CGRectMake(titleText.frame.size.width+titleText.frame.origin.y+30,6, self.bounds.size.width/2, 30)];
        [titleText2 setBackgroundColor:[UIColor clearColor]];
        titleText2.textAlignment = NSTextAlignmentCenter;
        NSDateFormatter *format = [[NSDateFormatter alloc] init];
        [format setDateFormat:@"YYYY"];
        NSString *dateString = [[format stringFromDate:self.calendarDate] uppercaseString];
        [titleText2 setText:dateString];
        [titleText2 setFont:[UIFont RDFontBig]];
        [titleText2 setTextColor:[UIColor whiteColor]];
        [titleText2 sizeToFit];
        UIImageView * imgv=[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"Forward"]];
        [titleText2 addSubview:imgv];
        [imgv setFrame:CGRectMake(titleText2.frame.size.width+2, titleText2.frame.size.height/2-imgv.frame.size.height/2, imgv.frame.size.width, imgv.frame.size.height)];
        
        
        UIImageView * imgv2=[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"Back"]];
        [titleText2 addSubview:imgv2];
        [imgv2 setFrame:CGRectMake(-2-imgv2.frame.size.width, titleText2.frame.size.height/2-imgv2.frame.size.height/2, imgv2.frame.size.width, imgv2.frame.size.height)];
        
        [titleText2 setCenter:CGPointMake(titleText.frame.origin.x+titleText.frame.size.width+40+titleText2.frame.size.width/2, titleText2.center.y+5)];
        
        
        
        
        
        [self addSubview:titleText2];
        UIButton * bLeft=[UIButton buttonWithType:UIButtonTypeCustom];
        
        [bLeft setFrame:CGRectMake(titleText2.frame.origin.x-15, titleText2.frame.origin.y-10, titleText2.frame.size.width/2+15, titleText2.frame.size.height+20)];
        [self addSubview:bLeft];
       // [bLeft setBackgroundColor:[UIColor grayColor]];
        [bLeft addTarget:self action:@selector(yearLeft) forControlEvents:UIControlEventTouchUpInside];
        UIButton * bRight=[UIButton buttonWithType:UIButtonTypeCustom];
        
        [bRight setFrame:CGRectMake(titleText2.frame.size.width/2+titleText2.frame.origin.x, titleText2.frame.origin.y-10, titleText2.frame.size.width/2+15, titleText2.frame.size.height+20)];
      //  [bRight setBackgroundColor:[UIColor purpleColor]];
        [self addSubview:bRight];
        [bRight addTarget:self action:@selector(yearRight) forControlEvents:UIControlEventTouchUpInside];
        
    }
    
    
    
    for (int i =0; i<_weekNames.count; i++) {
        UIButton *weekNameLabel = [UIButton buttonWithType:UIButtonTypeCustom];
        weekNameLabel.titleLabel.text = [_weekNames objectAtIndex:i];
        [weekNameLabel setTitle:[_weekNames objectAtIndex:i] forState:UIControlStateNormal];
        [weekNameLabel setFrame:CGRectMake(originX+(width*(i%columns)), originY, width, width)];
        [weekNameLabel setTitleColor:[UIColor RDGrayColor] forState:UIControlStateNormal];
        [weekNameLabel.titleLabel setFont:[UIFont RDFontMedium]];
        weekNameLabel.userInteractionEnabled = NO;
        [self addSubview:weekNameLabel];
    }
    

    for (NSInteger i= 0; i<monthLength; i++)
    {
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        button.tag = i+1;
        button.titleLabel.text = [NSString stringWithFormat:@"%d",i+1];
        [button setTitle:[NSString stringWithFormat:@"%d",i+1] forState:UIControlStateNormal];
        [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [button.titleLabel setFont:[UIFont RDFontMedium]];
      
        [button addTarget:self action:@selector(tappedDate:) forControlEvents:UIControlEventTouchUpInside];
        NSInteger offsetX = (width*((i+weekday)%columns));
        NSInteger offsetY = (height *((i+weekday)/columns));
        [button setFrame:CGRectMake(originX+offsetX, originY+spaceFromW+offsetY, width, height)];
        
          button.layer.cornerRadius=button.frame.size.height/2;
        
//        [button.layer setBorderColor:[[UIColor brownColor] CGColor]];
//        [button.layer setBorderWidth:2.0];
//        UIView *lineView = [[UIView alloc] init];
//        lineView.backgroundColor = [UIColor orangeColor];
        if(((i+weekday)/columns)==0)
        {
 //           [lineView setFrame:CGRectMake(0, 0, button.frame.size.width, 4)];
        //    [button addSubview:lineView];
        }

        if(((i+weekday)/columns)==((monthLength+weekday-1)/columns))
        {
   //         [lineView setFrame:CGRectMake(0, button.frame.size.width-4, button.frame.size.width, 4)];
           // [button addSubview:lineView];
        }
        
//        UIView *columnView = [[UIView alloc]init];
//        [columnView setBackgroundColor:[UIColor brownColor]];
        if((i+weekday)%7==0)
        {
  //          [columnView setFrame:CGRectMake(0, 0, 4, button.frame.size.width)];
   //         [button addSubview:columnView];
        }
        else if((i+weekday)%7==6)
        {
     //       [columnView setFrame:CGRectMake(button.frame.size.width-4, 0, 4, button.frame.size.width)];
//            [button addSubview:columnView];
        }
        if(i+1 ==_selectedDate && components.month == _selectedMonth && components.year == _selectedYear)
        {
            [button setBackgroundColor:[UIColor brownColor]];
            [button setTitleColor:[UIColor RDGreenColor] forState:UIControlStateNormal];
            
        }
        
        
            
        [self addSubview:button];
    }
    
    NSDateComponents *previousMonthComponents = [gregorian components:(NSEraCalendarUnit | NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit) fromDate:self.calendarDate];
    previousMonthComponents.month -=1;
    NSDate *previousMonthDate = [gregorian dateFromComponents:previousMonthComponents];
    NSRange previousMonthDays = [c rangeOfUnit:NSDayCalendarUnit
                   inUnit:NSMonthCalendarUnit
                  forDate:previousMonthDate];
    NSInteger maxDate = previousMonthDays.length - weekday;
    
    
    for (int i=0; i<weekday; i++) {
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        button.titleLabel.text = [NSString stringWithFormat:@"%d",maxDate+i+1];
        [button setTitle:[NSString stringWithFormat:@"%d",maxDate+i+1] forState:UIControlStateNormal];
        NSInteger offsetX = (width*(i%columns));
        NSInteger offsetY = (height *(i/columns));
        [button setFrame:CGRectMake(originX+offsetX, originY+spaceFromW+offsetY, width, height)];
//        [button.layer setBorderWidth:2.0];
//        [button.layer setBorderColor:[[UIColor brownColor] CGColor]];
//        UIView *columnView = [[UIView alloc]init];
//        [columnView setBackgroundColor:[UIColor brownColor]];
//        if(i==0)
//        {
//            [columnView setFrame:CGRectMake(0, 0, 4, button.frame.size.width)];
//            [button addSubview:columnView];
//        }

      //  UIView *lineView = [[UIView alloc]init];
     //   [lineView setBackgroundColor:[UIColor brownColor]];
     //   [lineView setFrame:CGRectMake(0, 0, button.frame.size.width, 4)];
       // [button addSubview:lineView];
        [button setTitleColor:[UIColor RDGrayColor] forState:UIControlStateNormal];
        [button.titleLabel setFont:[UIFont RDFontMedium]];
        [button setEnabled:NO];
        [self addSubview:button];
    }
    
    NSInteger remainingDays = (monthLength + weekday) % columns;
    if(remainingDays >0){
        for (int i=remainingDays; i<columns; i++) {
            UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
            button.titleLabel.text = [NSString stringWithFormat:@"%d",(i+1)-remainingDays];
            [button setTitle:[NSString stringWithFormat:@"%d",(i+1)-remainingDays] forState:UIControlStateNormal];
            NSInteger offsetX = (width*((i) %columns));
            NSInteger offsetY = (height *((monthLength+weekday)/columns));
            [button setFrame:CGRectMake(originX+offsetX, originY+spaceFromW+offsetY, width, height)];
//            [button.layer setBorderWidth:2.0];
//            [button.layer setBorderColor:[[UIColor brownColor] CGColor]];
      //      UIView *columnView = [[UIView alloc]init];
//            [columnView setBackgroundColor:[UIColor brownColor]];
//            if(i==columns - 1)
//            {
//                [columnView setFrame:CGRectMake(button.frame.size.width-4, 0, 4, button.frame.size.width)];
//                [button addSubview:columnView];
//            }
         //   UIView *lineView = [[UIView alloc]init];
         //   [lineView setBackgroundColor:[UIColor brownColor]];
         //   [lineView setFrame:CGRectMake(0, button.frame.size.width-4, button.frame.size.width, 4)];
         //   [button addSubview:lineView];
            [button setTitleColor:[UIColor RDGrayColor] forState:UIControlStateNormal];
            [button.titleLabel setFont:[UIFont RDFontMedium]];
            [button setEnabled:NO];
            [self addSubview:button];

        }
    }

}
-(IBAction)tappedDate:(UIButton *)sender
{
    gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *components = [gregorian components:(NSEraCalendarUnit | NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit) fromDate:self.calendarDate];
    if(!(_selectedDate == sender.tag && _selectedMonth == [components month] && _selectedYear == [components year]))
    {
        if(_selectedDate != -1)
        {
            UIButton *previousSelected =(UIButton *) [self viewWithTag:_selectedDate];
            [previousSelected setBackgroundColor:[UIColor clearColor]];
            [previousSelected setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            
        }
        
        [sender setBackgroundColor:[UIColor RDGreenColor]];
        [sender setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        _selectedDate = sender.tag;
        NSDateComponents *components = [gregorian components:(NSEraCalendarUnit | NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit) fromDate:self.calendarDate];
        components.day = _selectedDate;
        _selectedMonth = components.month;
        _selectedYear = components.year;
        NSDate *clickedDate = [gregorian dateFromComponents:components];
        [self.delegate tappedOnDate:clickedDate];
    }
}

-(void)swipeleft:(UISwipeGestureRecognizer*)gestureRecognizer
{
    
    [self.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    NSDateComponents *components = [gregorian components:(NSEraCalendarUnit | NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit) fromDate:self.calendarDate];
    components.day = 1;
    components.month += 1;
    self.calendarDate = [gregorian dateFromComponents:components];
    [UIView transitionWithView:self
                      duration:0.33f
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    animations:^ { [self setNeedsDisplay]; }
                    completion:nil];
    
    
}

-(void)swiperight:(UISwipeGestureRecognizer*)gestureRecognizer
{
    
    [self.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    NSDateComponents *components = [gregorian components:(NSEraCalendarUnit | NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit) fromDate:self.calendarDate];
    components.day = 1;
    components.month -= 1;
    self.calendarDate = [gregorian dateFromComponents:components];
    [UIView transitionWithView:self
                      duration:0.33f
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    animations:^ { [self setNeedsDisplay]; }
                    completion:nil];
}
-(void)setCalendarParameters
{
    if(gregorian == nil)
    {
        gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
        NSDateComponents *components = [gregorian components:(NSEraCalendarUnit | NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit) fromDate:self.calendarDate];
        _selectedDate  = components.day;
        _selectedMonth = components.month;
        _selectedYear = components.year;
    }
}

-(void)monthLeft{
    
     [self swiperight:nil];

}
-(void)monthRight{
      [self swipeleft:nil];
}
-(void)yearLeft{
    [self.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    NSDateComponents *components = [gregorian components:(NSEraCalendarUnit | NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit) fromDate:self.calendarDate];
    components.day = 1;
    components.year-=1;
    self.calendarDate = [gregorian dateFromComponents:components];
    [UIView transitionWithView:self
                      duration:0.33f
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    animations:^ { [self setNeedsDisplay]; }
                    completion:nil];
}
-(void)yearRight{
    [self.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    NSDateComponents *components = [gregorian components:(NSEraCalendarUnit | NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit) fromDate:self.calendarDate];
    components.day = 1;
    components.year+=1;
    self.calendarDate = [gregorian dateFromComponents:components];
    [UIView transitionWithView:self
                      duration:0.33f
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    animations:^ { [self setNeedsDisplay]; }
                    completion:nil];
}

@end
