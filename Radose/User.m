//
//  User.m
//  Radose
//
//  Created by mm7 on 18.07.14.
//  Copyright (c) 2014 mm7. All rights reserved.
//

#import "User.h"

@implementation User


-(void)saveUser{
    
    
    NSMutableDictionary * dict=[[NSMutableDictionary alloc]init];
    [dict setValue:_fullname forKey:@"fullname"];
    [dict setValue:_password forKey:@"password"];
    [dict setValue:_email forKey:@"email"];
    [dict setValue:self.objectId forKey:@"objectId"];
    
    [[NSUserDefaults standardUserDefaults] setValue:dict forKey:@"mainUser"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    
}

+(User*)loadUser{
    
       NSMutableDictionary * dict=[[NSUserDefaults standardUserDefaults] valueForKey:@"mainUser"];
    
    
    
    if (dict) {
        User * u=[[User alloc]init];
        u.password=[dict valueForKey:@"password"];
        u.email=[dict valueForKey:@"email"];
        u.fullname=[dict valueForKey:@"fullname"];
        u.objectId=[dict valueForKey:@"objectId"];
        
        return u;
        
    }else{
        return nil;
    }
    
    
    
}
@end
