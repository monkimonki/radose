//
//  RDErorr.m
//  Radose
//
//  Created by mm7 on 18.07.14.
//  Copyright (c) 2014 mm7. All rights reserved.
//

#import "RDErorr.h"

@implementation RDErorr
-(NSString *)erorrText{
    
    switch (_erorrType) {
        case kErorrTypeConnectionErorr:
            return @"connection erorr";
            break;
        case kErorrTypeDuplicateEmail:
            return @"Email";
            break;
            
        case kErorrTypeWrongEmailOrPassword:
            return @"wrongemailorpassword";
            break;
        default:
            break;
    }
    return @"??";
}
+(RDErorr *)createWithType:(enum kErorrTypes)t{
    
    
    RDErorr * er=[[RDErorr alloc] init];
    er.erorrType=t;
    
    return er;
}
@end
