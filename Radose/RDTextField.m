//
//  RDTextField.m
//  Radose
//
//  Created by mm7 on 18.07.14.
//  Copyright (c) 2014 mm7. All rights reserved.
//

#import "RDTextField.h"
@interface RDTextField(){
    UIImageView * leftImageView;
}
@end
@implementation RDTextField
+(RDTextField *)createBase{
    
    RDTextField * tf=[[self alloc]initWithFrame:CGRectMake(0, 0, 257, 37)];
    tf.leftOffset=12;
    [tf setPlaceHolderColor:[UIColor colorWithRed:1 green:1 blue:1 alpha:0.8]];
    [tf setTextColor:[UIColor whiteColor]];
    [tf setBaseColor:[UIColor whiteColor]];
    tf.errorFont=[UIFont RDFontSmall];

    return tf;
    
}
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {

        
        [self setBackgroundColor:[UIColor colorWithRed:1 green:1 blue:1 alpha:0.4]];
        self.layer.cornerRadius=2.0;
        
        [self setFont:[UIFont RDFontMedium]];
        
        
        
        
    }
    return self;
}

-(void)addImage:(UIImage *)img{
    
    if (!leftImageView) {
        leftImageView=[[UIImageView alloc]init];
        [leftImageView setFrame:CGRectMake(self.leftOffset, 0, self.frame.size.height*0.5, self.frame.size.height)];
        if (!img) {
            
            
            [leftImageView setBackgroundColor:[UIColor purpleColor]];
            
            
            
            
        }else{
            [leftImageView setContentMode:UIViewContentModeCenter];
            [leftImageView setImage:img];
            
        }
        
        [self addSubview:leftImageView];
    }
    
    
    
}

-(CGRect)textRectForBounds:(CGRect)bounds{
    if (leftImageView) {
        
        return [super textRectForBounds:CGRectMake(bounds.origin.x+leftImageView.frame.size.width+10, bounds.origin.y, bounds.size.width-leftImageView.frame.size.width-10, bounds.size.height)];
    }else{
        return  [super textRectForBounds:bounds];
    }
    
    
}
-(CGRect)editingRectForBounds:(CGRect)bounds{
    
    
    
    if (leftImageView) {
        
        return [super editingRectForBounds:CGRectMake(bounds.origin.x+leftImageView.frame.size.width+10, bounds.origin.y, bounds.size.width-leftImageView.frame.size.width-10, bounds.size.height)];
    }else{
        return  [super editingRectForBounds:bounds];
    }
    
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
