//
//  DPFViewController.m
//  Radose
//
//  Created by mm7 on 19.07.14.
//  Copyright (c) 2014 mm7. All rights reserved.
//

#import "DPFViewController.h"
#import "RDButton.h"
#import "GlobalTImePicker.h"
#import "SelectAirPortViewController.h"
#import "SelectDateViewController.h"
#import "AppDelegate.h"
#import <AVFoundation/AVAudioPlayer.h>
#import <AudioToolbox/AudioToolbox.h>
#import "ProgressView.h"
#import "ResultView.h"
#import "RDTabBarViewController.h"
#import "Calculation.h"
@interface DPFViewController ()<ProgressViewDelegare , GlobalTImePickerDelegate,SelectAirportDelegate,SelectDateDelgate>{
    UIButton * labelTime1;
    UIButton * labelTime2;
    
    UIButton *airButton1;
    UIButton * airButton2;
    UIButton *dateButton1;
    UIButton * dateButton2;
    UIImageView * planeImageView;
    
    UILabel * flightTime;
    int curTab;
    
    NSDate * time1;
    NSDate * time2;
    NSString * airId1;
    NSString * airId2;
    
    NSDate * date1;
    NSDate * date2;
    
    
    int ti1;
    int ti2;
    
    AVAudioPlayer *  shortEffect;
    
    float ratiation;
    ProgressView * pv;
    Calculation * calculation;
    ResultView * resultView;
}

@end

@implementation DPFViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [[RDNavigationController sharedInstance]showLogoWithAnimation:YES
                                                         andCompl:^{
                                                             
                                                         }];
    
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    calculation=[[Calculation alloc]init];
    
    NSString *soundFilePath = [[NSBundle mainBundle] pathForResource:@"4" ofType:@"mp3"];
    NSURL *soundFileURL = [NSURL fileURLWithPath:soundFilePath];
    //    player = [[AVAudioPlayer alloc] initWithContentsOfURL:soundFileURL error:nil];
    //    player.numberOfLoops = 2400; //infinite
    //    [player play];
    
    
    
    soundFilePath = [[NSBundle mainBundle] pathForResource:@"4" ofType:@"mp3"];
    soundFileURL = [NSURL fileURLWithPath:soundFilePath];
    shortEffect = [[AVAudioPlayer alloc] initWithContentsOfURL:soundFileURL error:nil];
    shortEffect.numberOfLoops = 0; //infinite
    UILabel * labelDepart=[[UILabel alloc]initWithFrame:CGRectMake(self.view.frame.size.width/4-115/2, 168, 115, 19)];
    [labelDepart setFont:[UIFont RDFontSmall]];
      [labelDepart setText:[locArrive localized]];
    [labelDepart setTextAlignment:NSTextAlignmentCenter];
    
    [labelDepart setTextColor:[UIColor whiteColor]];
    [self.view addSubview:labelDepart];
    [labelDepart setBackgroundColor:[UIColor clearColor]];
    
    UILabel * labelArrive=[[UILabel alloc]initWithFrame:CGRectMake(self.view.frame.size.width/2+ self.view.frame.size.width/4-115/2, 168, 115, 19)];
    [labelArrive setFont:[UIFont RDFontSmall]];
    [labelArrive setText:[locDepart localized]];
    [labelArrive setTextAlignment:NSTextAlignmentCenter];
        [labelArrive setBackgroundColor:[UIColor clearColor]];
        [labelArrive setTextColor:[UIColor whiteColor]];
        [self.view addSubview:labelArrive];
    
    
    
    
    
    
    labelTime1=[UIButton buttonWithType:UIButtonTypeCustom];
    [labelTime1 setFrame: CGRectMake(self.view.frame.size.width/4-100/2, [labelDepart getLastY], 100, 24)];
    [labelTime1.titleLabel setFont:[UIFont RDFontBig]];
    [labelTime1 setTitle:[locTime localized] forState:UIControlStateNormal];
    [labelTime1 setTitleColor:[UIColor RDYellowColor] forState:UIControlStateNormal];
    [labelTime1 addTarget:self action:@selector(selectTime1) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:labelTime1];
    
        [labelTime1 setBackgroundColor:[UIColor clearColor]];
    
    labelTime2=[UIButton buttonWithType:UIButtonTypeCustom];
    [labelTime2 setFrame: CGRectMake(self.view.frame.size.width/2+ self.view.frame.size.width/4-100/2, [labelDepart getLastY], 100, 24)];
    [labelTime2.titleLabel setFont:[UIFont RDFontBig]];
    [labelTime2 setTitle:[locTime localized] forState:UIControlStateNormal];
    [labelTime2 setTitleColor:[UIColor RDYellowColor] forState:UIControlStateNormal];
    [self.view addSubview:labelTime2];
   [labelTime2 addTarget:self action:@selector(selectTime2) forControlEvents:UIControlEventTouchUpInside];
    [labelTime2 setBackgroundColor:[UIColor clearColor]];
    
    
    
    airButton1=[UIButton buttonWithType:UIButtonTypeCustom];
    
    [airButton1 setFrame:CGRectMake(labelDepart.frame.origin.x+10, [labelTime1 getLastY], labelDepart.frame.size.width-20, 34)];
    [airButton1.titleLabel setFont:[UIFont RDFontMedium]];
    [airButton1 setTitle:[locAirport localized] forState:UIControlStateNormal];
    [airButton1 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [airButton1 addTarget:self action:@selector(selectAirPort1) forControlEvents:UIControlEventTouchUpInside];
    [airButton1.titleLabel setNumberOfLines:2];
    [airButton1 setContentHorizontalAlignment:UIControlContentHorizontalAlignmentCenter];
    [self.view addSubview:airButton1];
    
   
    airButton2=[UIButton buttonWithType:UIButtonTypeCustom];
    
    [airButton2 setFrame:CGRectMake(labelArrive.frame.origin.x+10, [labelTime1 getLastY], labelArrive.frame.size.width-20, 34)];
    [airButton2.titleLabel setFont:[UIFont RDFontSmall]];
    [airButton2 setTitle:[locAirport localized] forState:UIControlStateNormal];
    [airButton2 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [airButton2 addTarget:self action:@selector(selectAirPort2) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:airButton2];
    [airButton2.titleLabel setNumberOfLines:2];
    [airButton2 setContentHorizontalAlignment:UIControlContentHorizontalAlignmentCenter];
 
    
    ///-----------///
    
    planeImageView=[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"plane"]];
    [planeImageView setCenter:CGPointMake(self.view.frame.size.width/2, labelTime1.frame.size.height/2+labelTime1.frame.origin.y)];
    
    [self.view addSubview:planeImageView];
    
    ///-------///
    {
    dateButton1=[UIButton buttonWithType:UIButtonTypeCustom];
    
    [dateButton1 setFrame:CGRectMake(airButton1.frame.origin.x-10, [airButton1 getLastY], airButton1.frame.size.width+20, 34)];
  
    UIImageView * dImgv1=[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"calendar"]];
    [dImgv1 setFrame:CGRectMake(12, dateButton1.frame.size.height/2-dImgv1.frame.size.height/2, dImgv1.frame.size.width, dImgv1.frame.size.height)];
    
    [dateButton1 addSubview:dImgv1];
    
    UILabel * dLabel1=[[UILabel alloc]initWithFrame:CGRectMake(dImgv1.frame.size.width+dImgv1.frame.origin.x+4, 0,dateButton1.frame.size.width-(dImgv1.frame.size.width+dImgv1.frame.origin.x)*2+10, dateButton1.frame.size.height)];
    [dLabel1 setFont:[UIFont RDFontSmall]];
    [dLabel1 setText:[locDate localized]];
    [dLabel1 setTextColor:[UIColor whiteColor]];
            [dLabel1 setTag:1];
    [dLabel1 setTextAlignment:NSTextAlignmentLeft];
    [dateButton1 addSubview:dLabel1];
    [dateButton1 addTarget:self action:@selector(selectDate1) forControlEvents:UIControlEventTouchUpInside];
    
    
    
    [self.view addSubview:dateButton1];
    }
  
    {
        dateButton2=[UIButton buttonWithType:UIButtonTypeCustom];
        
        [dateButton2 setFrame:CGRectMake(airButton2.frame.origin.x-10, [airButton2 getLastY], airButton2.frame.size.width+20, 34)];
        
        UIImageView * dImgv1=[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"calendar"]];
        [dImgv1 setFrame:CGRectMake(24, dateButton2.frame.size.height/2-dImgv1.frame.size.height/2, dImgv1.frame.size.width, dImgv1.frame.size.height)];
        
        [dateButton2 addSubview:dImgv1];
        
        UILabel * dLabel1=[[UILabel alloc]initWithFrame:CGRectMake(dImgv1.frame.size.width+dImgv1.frame.origin.x+4, 0,dateButton1.frame.size.width-(dImgv1.frame.size.width+dImgv1.frame.origin.x)*2+22, dateButton2.frame.size.height)];
        [dLabel1 setFont:[UIFont RDFontSmall]];
        [dLabel1 setText:[locDate localized]];
        [dLabel1 setTextColor:[UIColor whiteColor]];
        [dLabel1 setTextAlignment:NSTextAlignmentLeft];
        [dLabel1 setTag:1];
        [dateButton2 addSubview:dLabel1];
        [dateButton2 addTarget:self action:@selector(selectDate2) forControlEvents:UIControlEventTouchUpInside];
        
        
        
        [self.view addSubview:dateButton2];
    }
    
    flightTime =[[UILabel alloc]initWithFrame:CGRectMake(dateButton1.frame.size.width+dateButton1.frame.origin.x-16, dateButton1.frame.origin.y-20, self.view.frame.size.width-(dateButton1.frame.size.width+dateButton1.frame.origin.x)*2+32, dateButton1.frame.size.height)];
    [flightTime setFont:[UIFont RDFontSmall]];
    [flightTime setNumberOfLines:2];
    [flightTime setTextColor:[UIColor whiteColor]];
    [flightTime setTextAlignment:NSTextAlignmentCenter];
    [flightTime setText:@""];
    
    [self.view addSubview:flightTime];
    
///////
    
    
    RDButton * clearBut=[RDButton createWithTitle:[locClear localized]];
    
    [clearBut setFrame:CGRectMake(32,[flightTime getLastY]+5+38/2, 126,38)];
    [clearBut addTarget:self action:@selector(clear) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:clearBut];
    
    
    RDButton * calcBut=[RDButton createWithTitle:[locCalculate localized]];
    [calcBut setGreen];
    [calcBut setFrame:CGRectMake(self.view.frame.size.width-32-126,[flightTime getLastY]+5+38/2, 126,38)];
    [calcBut addTarget:self action:@selector(calculate) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:calcBut];
    //
    
    
     pv=[[ProgressView alloc]initWithFrame:CGRectMake(34, [calcBut getLastY]+10, self.view.frame.size.width-68, 38)];
    [pv setDelegate:self];
    [pv setPercent:0];
    [self.view addSubview:pv];
    
    
    
    
    resultView =[[ResultView alloc] initWithFrame:CGRectMake(pv.frame.origin.x, [pv getLastY]+10, pv.frame.size.width, 77)];
        [self.view addSubview:resultView];
    
    UIButton * but=[UIButton buttonWithType:UIButtonTypeCustom];
    
    [but setFrame:resultView.frame];
    [but addTarget:self action:@selector(saveCalc) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:but];
    
    // Do any additional setup after loading the view.
}
-(void)saveCalc{
    if (resultView.alpha==0) {
        return;
    }
    
    
    
    [[RDSingleton sharedInstance]saveUserCalculation:calculation andBlock:^(BOOL suc, NSString *calcId, RDErorr *err) {
        
        
        RDTabBarViewController *t=self.navigationController.tabBarController;
        
        [t selectIndex:2];
        
        
        
    }];
 
    
     
    
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)timeAnimation{
 
        CABasicAnimation *animation =
        [CABasicAnimation animationWithKeyPath:@"position"];
        [animation setDuration:0.02];
        [animation setRepeatCount:2];
        [animation setAutoreverses:YES];
        [animation setFromValue:[NSValue valueWithCGPoint:
                                 CGPointMake([planeImageView center].x -2, [planeImageView center].y)]];
        [animation setToValue:[NSValue valueWithCGPoint:
                               CGPointMake([planeImageView center].x +2, [planeImageView center].y)]];
        [[planeImageView layer] addAnimation:animation forKey:@"position"];
        
            
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
#pragma mark - func
-(void)selectTime1{
    GlobalTImePicker * tp = [GlobalTImePicker createGlobalTimePicker];
    [tp setPickerDelegate:self];
    curTab=0;
}
-(void)selectTime2{
    GlobalTImePicker * tp = [GlobalTImePicker createGlobalTimePicker];
    [tp setPickerDelegate:self];
    curTab=1;
}

-(void)selectAirPort1{
    curTab=0;
    SelectAirPortViewController * avc=[[SelectAirPortViewController alloc]init];
    [avc setDelegate:self];
    [self.navigationController pushViewController:avc animated:YES];
}
-(void)selectAirPort2{
    curTab=1;
    SelectAirPortViewController * avc=[[SelectAirPortViewController alloc]init];
    [avc setDelegate:self];
    [self.navigationController pushViewController:avc animated:YES];
}
-(void)selectDate1{
    curTab=0;
    
    SelectDateViewController   *s =[[SelectDateViewController alloc]init];
    
    [s setDelegate:self];
    [self.navigationController pushViewController:s animated:YES];
}
-(void)selectDate2{
    curTab=1;
    
    
    SelectDateViewController   *s =[[SelectDateViewController alloc]init];
    
    [s setDelegate:self];
    [self.navigationController pushViewController:s animated:YES];
}

-(void)clear{
    [labelTime1 setTitle:[locTime localized] forState:UIControlStateNormal];
    [labelTime2 setTitle:[locTime localized] forState:UIControlStateNormal];
    [airButton1 setTitle:[locAirport localized] forState:UIControlStateNormal];
    [airButton2 setTitle:[locAirport localized] forState:UIControlStateNormal];
    UILabel * l1=(UILabel*) [dateButton1 viewWithTag:1];
    [l1 setText:[locDate localized]];
    UILabel * l2=(UILabel*) [dateButton2 viewWithTag:1];
      [l2 setText:[locDate localized]];
    [flightTime setText:@""];
    
    date1=nil;
    date1=nil;
    airId1=nil;
    airId2=nil;
    time1=nil;
    time2=nil;
    [pv setPercent:0];
    calculation.ti2=nil;
    calculation.ti1=nil;
    calculation.air1=nil;
    calculation.air2=nil;
    
    [UIView animateWithDuration:0.33 animations:^{
        
        [resultView setAlpha:0];
    }];
}
-(void)play{
    shortEffect.currentTime=4;
    [shortEffect play];
}
-(void)stop{
    [shortEffect stop];
}
-(void)calculate{
    if (!date1||!date2||!time1||!time2||!airId1||!airId2) {
        return;
    }
    
    [UIView animateWithDuration:0.33 animations:^{
        
        [resultView setAlpha:0];
    }];
 AppDelegate * ap=(AppDelegate*)   [UIApplication sharedApplication].delegate;
    CABasicAnimation *animation =
    [CABasicAnimation animationWithKeyPath:@"position"];
    [animation setDuration:0.02];
    [animation setRepeatCount:10000];
    [animation setAutoreverses:YES];
    [animation setFromValue:[NSValue valueWithCGPoint:
                             CGPointMake([ap.window center].x -8, [ap.window center].y+4)]];
    [animation setToValue:[NSValue valueWithCGPoint:
                           CGPointMake([ap.window center].x +8, [ap.window center].y-4)]];
    
   ;
    [ap.window setUserInteractionEnabled:NO];
    [[ap.window layer] addAnimation:animation forKey:@"position"];
    [self play];
    [[RDSingleton sharedInstance]calculate:ti1 airId1:airId1 ti2:ti2 airId2:airId2 andCompBloc:^(BOOL suc, NSString *result, RDErorr *err) {
        NSNumber * n=result;
        
        float a=[n floatValue];
        
        
        [resultView setRadiance:a];
        ratiation=a/(10*10);
        NSLog(@"%f",ratiation);
        
            [ap.window setUserInteractionEnabled:YES];
            [ap.window.layer performSelector:@selector(removeAllAnimations) withObject:nil afterDelay:1.0];
        [self performSelector:@selector(stop) withObject:nil afterDelay:1.3];
        
            [pv setRadiance:[n floatValue]];
        
    }];
    
}

-(void)didFinishProgress{
    
    [UIView animateWithDuration:0.33 animations:^{
       
        [resultView setAlpha:1];
    }];
    
}

#pragma mark timePiker

-(void) picked: (NSString *) val{
    if (curTab==0) {
        [labelTime1 setTitle:val forState:UIControlStateNormal];
    }else{
        [labelTime2 setTitle:val forState:UIControlStateNormal];
    }
   
}
#pragma mark air port delegate
-(void)didSelectAirpord:(NSDictionary *)d{
    [self.navigationController popViewControllerAnimated:YES];
    NSString * t;
        if ( [[[NSLocale preferredLanguages] objectAtIndex:0] isEqualToString:@"ru"]) {
            t=[NSString stringWithFormat:@"%@, %@",[d valueForKey:@"iata_code" ],[d valueForKey:@"city_rus"]];
        }else{
                        t=[NSString stringWithFormat:@"%@, %@",[d valueForKey:@"iata_code" ],[d valueForKey:@"city_eng"]];
        }
    
    if (curTab==0) {
        airId1=[d valueForKey:@"k_airport_id"];
        [airButton1 setTitle:t forState:UIControlStateNormal];
        
        calculation.air1=[d valueForKey:@"k_airport_id"];
        
        
    }else{
        
                calculation.air2=[d valueForKey:@"k_airport_id"];
                airId2=[d valueForKey:@"k_airport_id"];
          [airButton2 setTitle:t forState:UIControlStateNormal];
    }
}

-(void)pickedDate:(NSDate *)val{
    if (curTab==0) {
        time1=val;
    }else{
        time2=val;
    }
         [self setIntervals];
}

-(void)setIntervals{
    if (!date1||!date2||!time1||!time2) {
        return;
    }
    
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];

    
    NSDateComponents *dateC1 = [gregorian components:(NSEraCalendarUnit | NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit) fromDate:date1];
    
    NSDateComponents *timeC1 = [gregorian components:( NSHourCalendarUnit | NSMinuteCalendarUnit) fromDate:time1];
    dateC1.hour=timeC1.hour;
    dateC1.minute=timeC1.minute;
    
    NSDate *readyDate1 = [gregorian dateFromComponents:dateC1];
    
    
    
    
    
    NSDateComponents *dateC2 = [gregorian components:(NSEraCalendarUnit | NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit) fromDate:date2];
    
    NSDateComponents *timeC2 = [gregorian components:( NSHourCalendarUnit | NSMinuteCalendarUnit) fromDate:time2];
    dateC2.hour=timeC2.hour;
    dateC2.minute=timeC2.minute;
    
    NSDate *readyDate2 = [gregorian dateFromComponents:dateC2];
    
  int a=  [readyDate2 timeIntervalSinceDate:readyDate1];
    
    NSString * tS=@"";
    if (a<60) {
        tS=[NSString stringWithFormat:@"%ds",a];
    }else{
        if (a<3600) {
            tS=[NSString stringWithFormat:@"%dm",a/60];
        }else{
            
            int m=a%3600;
            int h=(a-m)/3600;
                        tS=[NSString stringWithFormat:@"%dh %dm",h,m/60];
            
        }
    }
    ti1=[readyDate1 timeIntervalSince1970];
    ti2=[readyDate2 timeIntervalSince1970];
       calculation.ti1=[NSNumber numberWithInteger:[readyDate1 timeIntervalSince1970]];
      calculation.ti2=[NSNumber numberWithInteger:[readyDate2 timeIntervalSince1970]];
    [flightTime setText:[NSString stringWithFormat:@"%@\n%@",tS,[locFlightTime localized]]];
    
}


-(void)didSelectDate:(NSDate *)date{
    [self.navigationController popViewControllerAnimated:YES];
    NSDateFormatter * df=[[NSDateFormatter alloc]init];
    [df setDateFormat:@"dd-MMMM-YYYY"];
    UILabel * lab;
    if (curTab==0) {
        date1=date;
     lab=(UILabel*)   [dateButton1 viewWithTag:1];
        
    }else{
        date2=date;
                 lab=(UILabel*)   [dateButton2 viewWithTag:1];
    }

    
    
      [lab setText:[df stringFromDate:date]];
    

    [self setIntervals];
    
}
@end
