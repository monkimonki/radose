//
//  ArportUpdater.m
//  Radose
//
//  Created by mm7 on 19.07.14.
//  Copyright (c) 2014 mm7. All rights reserved.
//

#import "ArportUpdater.h"
#import "AppDelegate.h"
@implementation ArportUpdater
+(void)updateAirPorts:(void(^)(BOOL suc,RDErorr*err))block{
    AppDelegate * d=[UIApplication sharedApplication].delegate;
    
    UIWindow *w=d.window;
    
    ArportUpdater * a=[[ArportUpdater alloc]initWithFrame:w.bounds];
    
    
    [w addSubview:a];
    
    
    [a setAlpha:0];
    
    [UIView animateWithDuration:0.33 animations:^{
        [a setAlpha:1];
    }];
    
     [[RDSingleton sharedInstance]loadAirports:^(BOOL suc, RDErorr *err) {
         
         if (suc) {
             [a removeFromSuperview];
         }else{
             [a setErorr:err.erorrText];
             [UIView animateWithDuration:1 animations:^{
                 [a setAlpha:0.8];
             }completion:^(BOOL finished) {
                 [UIView animateWithDuration:0.5 animations:^{
                     [a setAlpha:0.0];
                 }completion:^(BOOL finished) {
                     [a removeFromSuperview];
                 }];
             }];
             
         }
         
         block (suc,err);
         
     }];
    
    
    
}
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setBackgroundColor:[UIColor colorWithRed:0 green:0 blue:0 alpha:0.84]];
     
        
        UIActivityIndicatorView * av=[[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
        
        [av setCenter:CGPointMake(self.frame.size.width/2, self.frame.size.height/2)];
        [av startAnimating];
        [self addSubview:av];
        
        UILabel * label=[[UILabel alloc]initWithFrame:CGRectMake(15, 15, self.frame.size.width-30, self.frame.size.height-self.frame.size.height/2-30)];
        [label setText:[locAirPortsUpdating localized]];
        [label setFont:[UIFont RDFontMedium]];
        [label setTextAlignment:NSTextAlignmentCenter];
        [label setNumberOfLines:6];
        [label setTextColor:[UIColor whiteColor]];
        [label setBackgroundColor:[UIColor clearColor]];
        
        [self addSubview:label];
        [label setTag:1];
        
        [av setTag:2];
        
        
    }
    return self;
}
-(void)setErorr:(NSString *)er{
    [[self viewWithTag:2]removeFromSuperview];
    
    UILabel * lab=[self viewWithTag:1];
    
    [lab setText:er];
    
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
