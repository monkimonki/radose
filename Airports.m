//
//  Airports.m
//  Radose
//
//  Created by mm7 on 19.07.14.
//  Copyright (c) 2014 mm7. All rights reserved.
//

#import "Airports.h"


@implementation Airports
@synthesize city_eng,city_rus,country_eng,country_rus,name_eng,name_rus,icao_code,iata_code;
-(void)setDict:(NSDictionary *)dict{
    
    city_rus=[dict valueForKey:@"city_rus"];
    city_eng=[dict valueForKey:@"city_eng"];
    country_rus=[dict valueForKey:@"country_rus"];
    country_eng=[dict valueForKey:@"country_eng"];
    name_eng=[dict valueForKey:@"name_eng"];
    name_rus=[dict valueForKey:@"name_rus"];
    icao_code=[dict valueForKey:@"icao_code"];
    iata_code=[dict valueForKey:@"iata_code"];
    
}
@end
